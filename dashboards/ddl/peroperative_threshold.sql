
drop table if exists reporting.peroperative_threshold;

create table if not exists reporting.peroperative_threshold (
	person_id 				integer not null
	, visit_occurrence_id 	integer not null
	, visit_detail_id 		integer not null
	, visit_detail_start_date date
	, brady_60_anesthesia	integer
	, duration_brady_60_anesthesia numeric
	, tachy_120_anesthesia	integer
	, duration_tachy_120_anesthesia numeric
	, hypo_65_anesthesia	integer
	, duration_hypo_65_anesthesia numeric
	, hyper_120_anesthesia	integer
	, duration_hyper_120_anesthesia numeric
	, desat_90_anesthesia	integer
	, duration_desat_90_anesthesia numeric
);




delete from reporting.peroperative_threshold;

with operation as(
	select person_id
	, visit_occurrence_id
	, visit_detail_id
	, visit_detail_start_date
	from omop.visit_detail vd
	where vd.visit_detail_concept_id = 2100000000
), brady_60_threshold as (
	select o.visit_detail_id,
	case 
	when value_as_concept_id = 4188539 then 1
	when value_as_concept_id = 4188540 then 0
	else null
	end as brady_60_anesthesia,
	m.value_as_number as duration_brady_60_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '87'
	and c.vocabulary_id = 'Lille Threshold'
), tachy_120_threshold as (
	select o.visit_detail_id,
	case 
	when value_as_concept_id = 4188539 then 1
	when value_as_concept_id = 4188540 then 0
	else null
	end as tachy_120_anesthesia,
	m.value_as_number as duration_tachy_120_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '120'
	and c.vocabulary_id = 'Lille Threshold'
), hypo_65_threshold as (
	select o.visit_detail_id,
	case 
	when value_as_concept_id = 4188539 then 1
	when value_as_concept_id = 4188540 then 0
	else null
	end as hypo_65_anesthesia,
	m.value_as_number as duration_hypo_65_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '100'
	and c.vocabulary_id = 'Lille Threshold'
), hyper_120_threshold as (
	select o.visit_detail_id,
	case 
	when value_as_concept_id = 4188539 then 1
	when value_as_concept_id = 4188540 then 0
	else null
	end as hyper_120_anesthesia,
	m.value_as_number as duration_hyper_120_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '92'
	and c.vocabulary_id = 'Lille Threshold'
), desat_90_threshold as (
	select o.visit_detail_id,
	case 
	when value_as_concept_id = 4188539 then 1
	when value_as_concept_id = 4188540 then 0
	else null
	end as desat_90_anesthesia,
	m.value_as_number as duration_desat_90_anesthesia
	from operation o 
	left outer join omop.measurement m
	on o.visit_detail_id = m.visit_detail_id
	inner join omop.concept c
	on m.measurement_source_concept_id = c.concept_id
	and c.concept_code = '2'
	and c.vocabulary_id = 'Lille Threshold'
)
insert into reporting.peroperative_threshold
select o.person_id::integer
		, o.visit_occurrence_id::integer
		, o.visit_detail_id::integer
		, o.visit_detail_start_date::date
		, brady_60_threshold.brady_60_anesthesia
		, brady_60_threshold.duration_brady_60_anesthesia
		, tachy_120_threshold.tachy_120_anesthesia
		, tachy_120_threshold.duration_tachy_120_anesthesia
		, hypo_65_threshold.hypo_65_anesthesia
		, hypo_65_threshold.duration_hypo_65_anesthesia
		, hyper_120_threshold.hyper_120_anesthesia
		, hyper_120_threshold.duration_hyper_120_anesthesia
		, desat_90_threshold.desat_90_anesthesia
		, desat_90_threshold.duration_desat_90_anesthesia
from operation o
left outer join brady_60_threshold
on o.visit_detail_id = brady_60_threshold.visit_detail_id
left outer join tachy_120_threshold
on o.visit_detail_id = tachy_120_threshold.visit_detail_id
left outer join hypo_65_threshold
on o.visit_detail_id = hypo_65_threshold.visit_detail_id
left outer join hyper_120_threshold
on o.visit_detail_id = hyper_120_threshold.visit_detail_id
left outer join desat_90_threshold
on o.visit_detail_id = desat_90_threshold.visit_detail_id
;

select * from reporting.peroperative_threshold 
--where mean_etd_surgery is not null
limit 100;

select count(*) from reporting.peroperative_indicator
limit 100;



