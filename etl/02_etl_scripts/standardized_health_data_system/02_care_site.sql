----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Table : CARE_SITE, FACT_RELATIONSHIP
--
-- Antoine Lamer
--
-- 25/12/19
--
-- 1) Extract hospitals, medical units, operating rooms
-- 2) Transform and mapp to OHDSI concept_id, map to fact_relationship
-- 3) Load into omop.care_site
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- Care site
------------------------------------------------------------


-- 1) Extract care sites' name
-- from the source table
------------------------------------------------------------

drop table if exists etl.care_site_extract;

create table if not exists etl.care_site_extract
as
-- Raw data - CAC
-- select cac as care_site_name
--, code_cac as care_site_source_value
--from raw_data.structure_chul
--union 
-- Raw data - UF
select uf as care_site_name
, code_uf as care_site_source_value
from raw_data.structure_chul
group by uf, code_uf
-- Raw data - Service
-- union
-- select service as care_site_name
-- , code_service as care_site_source_value
-- from raw_data.structure_chul
-- Raw data - Clinique
-- union
-- select clinique as care_site_name
-- , code_clinique as care_site_source_value
-- from raw_data.structure_chul
--  Raw data - Pole
-- union
-- select pole as care_site_name
-- , code_pole as care_site_source_value
-- from raw_data.structure_chul
--
-- Operative room
--
union 
select salle as care_site_name
, salle_nm::character as care_site_source_value
from raw_data.intervention_patient
group by salle, salle_nm
--
-- Specialty units
--
union
select service as care_site_name
, service_nm::character as care_site_source_value
from raw_data.intervention_patient
group by service, service_nm
;

insert into etl.care_site_extract (care_site_name, care_site_source_value) 
values ( 'CHU Lille', 'CHU Lille');


-- 2. Transform
--
-- location_id
-- place_of_service_concept_id
--
----------------------------------------------------

-- 3. Load into omop_anesthesia.visit_occurrence
----------------------------------------------------

truncate omop.care_site;
alter sequence etl.care_site_id_seq restart with 1;

insert into omop.care_site (
	care_site_id
	, care_site_name
	, place_of_service_concept_id
	, location_id
	, care_site_source_value
	, place_of_service_source_value
)
-----
select nextval('etl.care_site_id_seq')
	, care_site_name
	, 0
	, coalesce((select min(location_id) from omop.location where location_source_value = 'CHU Lille'), 0) as location_id
	, care_site_source_value
	, 0
from etl.care_site_extract;



-- 4. Load FACT_RELATIONSHIP
----------------------------------------------------

-- 46233688 Care Site is part of Care Site
-- 46233689 Care Site contains Care Site
-- 57 Care site

delete from omop.fact_relationship where relationship_concept_id in (46233688, 46233689);

-- 4.1 CAC and UF

select
code_cac, cac,
cac.care_site_id, cac.care_site_name, cac.care_site_source_value,
code_uf, uf,
uf.care_site_id, uf.care_site_name, uf.care_site_source_value
from raw_data.structure_chul sc
left outer join omop.care_site cac
on sc.code_cac = cac.care_site_source_value
and sc.cac = cac.care_site_name
left outer join omop.care_site uf
on sc.code_uf = uf.care_site_source_value
and sc.uf = uf.care_site_name
;


-- 4.2 Units and rooms for anesthesia procedure and consultations
-- Get the rooms, the units, and the care_site_id of each one

with service_salle as (
	select service, service_nm
	, salle, salle_nm
	from raw_data.intervention_patient
	group by service, service_nm, salle, salle_nm
     ), care_site_service_salle as (
	-- get care_site_id for each room and unit
    select service, service_nm, omop_service.care_site_id as service_care_site_id,
	salle, salle_nm, omop_salle.care_site_id as salle_care_site_id
    from service_salle
	-- join for the unit care_site_id
	left outer join omop.care_site as omop_service
	on service_salle.service = omop_service.care_site_name
	and service_salle.service_nm::character = omop_service.care_site_source_value
	-- join for the room care_site_id
	left outer join omop.care_site as omop_salle
	on service_salle.salle = omop_salle.care_site_name
	and service_salle.salle_nm::character = omop_salle.care_site_source_value
    )
insert into omop.fact_relationship (domain_concept_id_1, fact_id_1, domain_concept_id_2, fact_id_2, relationship_concept_id)
select 57 -- Domain Care site
, service_care_site_id
, 57 -- Domain Care site
, salle_care_site_id
, 46233689 --Care Site contains Care Site
FROM care_site_service_salle
union
select 57 -- Domain Care site
, salle_care_site_id
, 57 -- Domain Care site
, service_care_site_id
, 46233688 -- Care Site is part of Care Site
FROM care_site_service_salle
;
