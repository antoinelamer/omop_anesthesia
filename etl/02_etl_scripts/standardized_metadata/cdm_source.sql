----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Table : CDM_SOURCE
--
-- Antoine Lamer
--
-- 23/03/2020
--
--
----------------------------------------


INSERT INTO omop.cdm_source(
	cdm_source_name, 
	cdm_source_abbreviation, 
	cdm_holder, 
	source_description, 
	source_documentation_reference, 
	cdm_etl_reference, 
	source_release_date, 
	cdm_release_date, 
	cdm_version, 
	vocabulary_version
)
VALUES (
	'Lille University Hospital', 
	'CHU Lille', 
	'EA2694', 
	'Hospital data of patient who had an anesthesia procedure between 2010 and 2019', 
	NULL::text, 
	'https://pad.interhop.org/vxaBg1wjT-y70THA2MzV2w', 
	'2019-12-31', 
	'2019-02-06', 
	'6.0.0', 
	'2019-12-19');
	
	