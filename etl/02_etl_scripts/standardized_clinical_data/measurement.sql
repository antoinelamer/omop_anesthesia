----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 2020-03-28
--
-- 1) Extract raw data
-- 2) Transform and map to standard concept_id
-- 3) Load into omop_anesthesia.visit_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

------------------------------------------------------------
-- Source 1 : Measurement 
-- heart rate, arterial pressure, ...
------------------------------------------------------------


-- 1) Extract measurement value, parameter, unit, date and 
-- operation identifier from the source table
------------------------------------------------------------

drop table if exists etl.measurement_extract;

create table if not exists etl.measurement_extract as 
--
select id_intervention::text as visit_detail_source_value
		, id_parametre::text as measurement_source_value
		, valeur as value_source_value
		, date_sauvegarde as measurement_datetime
		, id_unite::text as unit_source_value
from raw_data.interv_mesure;

select * from etl.measurement_extract;

-- 2) Transform
--
-- Mapping with concept_id
-- 
-- person_id
-- visit_occurrence_id
-- visit_detail_id
--
-- Value, keep only numeric value ?
-- 
----------------------------------------------------

-- visit_detail_id
-- measurement_source_concept_id
-- unit_source_concept_id

drop table if exists etl.measurement_tr_1;

create table if not exists etl.measurement_tr_1 as 
select 	0 as measurement_id
		, coalesce(vd.person_id, 0) as person_id
		, 0::integer as measurement_concept_id
		, measurement_datetime::date as measurement_date
		, measurement_datetime
		, null::varchar as measurement_time
		, (select min(concept_id) from omop.concept where vocabulary_id = 'Meas Type' and concept_name = 'From monitor' ) as measurement_type_concept_id
		, null::integer as operator_concept_id
		, value_source_value::numeric as value_as_number -- keep only numeric value
		, null::integer as value_as_concept_id
		, 0::integer as unit_concept_id
		, null::numeric as range_low
		, null::numeric as range_high
		, 0 as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, measurement_source_value
		, coalesce(cm.concept_id, 0) as measurement_source_concept_id
		, unit_source_value
		, coalesce(cu.concept_id, 0) as unit_source_concept_id
		, value_source_value::text as value_source_value
from etl.measurement_extract me
-- visit_detail_id
left outer join omop.visit_detail vd
on me.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 
	(select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit')
-- measurement_source_concept_id
left outer join omop.concept cm
on me.measurement_source_value = cm.concept_code
and cm.vocabulary_id = 'Lille AIMS Meas'
-- unit_source_concept_idit
left outer join omop.concept cu
on me.unit_source_value = cu.concept_code
and cu.vocabulary_id = 'Lille AIMS Unit'
;

--
-- Measurement_concept_id
-- Unit_concept_id
-------------------------------------------------------------------------------

drop table if exists etl.measurement_tr;

create table if not exists etl.measurement_tr as 
select 	0 as measurement_id
		, person_id
		, coalesce(cr1.concept_id_2, 0) as measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id  -- TODO
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, coalesce(cr2.concept_id_2, 0) as unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_tr_1 tr1
-- measurement_concept_id
left outer join omop.concept_relationship cr1
on tr1.measurement_source_concept_id = cr1.concept_id_1
-- unit_concept_id
left outer join omop.concept_relationship cr2
on tr1.unit_source_concept_id = cr2.concept_id_1
;

-------------------------------------------------------------------------------
-- Source 3 : threshold 
-- ex : MAP < 65 during surgery, ...
-------------------------------------------------------------------------------

-- 1)
--
-------------------------------------------------------------------------------

create table if not exists etl.measurement_threshold_extract as
select id_intervention::text as visit_detail_source_value
		, id_seuil::text as measurement_source_value
		, date_evt_declencheur_fen as measurement_datetime
		, temps_hors_seuil as value_as_number
		, case
		when b_temps_hors_seuil = 0 then 4188540 -- no
		when b_temps_hors_seuil = 1 then 4188539 -- yes
		else 4080894 -- null
		end as value_as_concept_id
		, temps_hors_seuil as value_source_value
from raw_data.interv_seuil;

-- 2)
-- Mapping with person_id, visit_detail_id, visit_occurrence_id
-- measurement_source_concept_id
-------------------------------------------------------------------------------

drop table if exists etl.measurement_threshold_tr;

create table if not exists etl.measurement_threshold_tr as
select 0::integer as measurement_id
		, coalesce(vd.person_id, 0) as person_id
		, 0::integer as measurement_concept_id
		, measurement_datetime::date as measurement_date
		, mte.measurement_datetime
		, null::varchar as measurement_time
		, 0::integer as measurement_type_concept_id -- todo
		, null::integer as operator_concept_id
		, mte.value_as_number
		, mte.value_as_concept_id
		, null::integer as unit_concept_id
		, null::numeric as range_low
		, null::numeric as range_high
		, 0 as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, mte.measurement_source_value as measurement_source_value
		, c.concept_id as measurement_source_concept_id
		, null::varchar as unit_source_value
		, null::varchar as value_source_value
		, mte.visit_detail_source_value
from etl.measurement_threshold_extract mte
-- visit_detail_id
left outer join omop.visit_detail vd
on mte.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 
	(select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit')
-- measurement_source_concept_id
left outer join omop.concept c
on mte.measurement_source_value = c.concept_code
and c.vocabulary_id = 'Lille Threshold'
;

-- 3) Load into omop.measurement
----------------------------------------------------

delete from omop.measurement where measurement_source_concept_id 
	in (select concept_id from omop.concept where vocabulary_id = 'Lille AIMS Mea')
	;
	
alter sequence etl.measurement_id_seq restart with 1;

insert into omop.measurement (
		measurement_id
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
)
-----
select nextval('etl.measurement_id_seq')
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_tr
;