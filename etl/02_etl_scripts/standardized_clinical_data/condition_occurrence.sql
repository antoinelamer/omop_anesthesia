----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Table : CONDITION_OCCURRENCE
--
-- Antoine Lamer
--
-- 14/12/19
--
-- Sources :
--
--
-- 1) Extract patient_id, ...
-- 2) Transform and map to standard concept_id
-- 3) Load into omop.condition_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- Person
------------------------------------------------------------


-- 1. Extract visit_start_date, visit_end_date, visit_source_value,
-- admitted_from_source_value, discharge_to_source_value
-- from the source table
------------------------------------------------------------
drop table if exists etl.condition_occurrence_extract;

create table if not exists etl.condition_occurrence_extract
as
-- Raw data -- Operating room
select 
		trim(iep) as visit_source_value
		, id_mouvement::text as visit_detail_source_value -- temporary field
		, type_diagnostic as condition_type_source_value
		, SPLIT_PART(REPLACE(code_diagnostic, '*', ''), ' ', 1) as condition_source_value
from raw_data.diagnostic;

-- 2.1 Transform
--
-- get person_id, visit_occurrence_id, visit_detail_id
--
----------------------------------------------------

drop table if exists etl.condition_occurrence_tr1;

create table if not exists etl.condition_occurrence_tr1
as
select 0 as	condition_occurrence_id
	, coalesce(person_id, 0) as person_id
	, 0 as condition_concept_id  -- TODO
	, coalesce(vd.visit_detail_start_date, null::date) as condition_start_date
	, coalesce(vd.visit_detail_start_date, null::date ) as condition_start_datetime
	, coalesce(vd.visit_detail_end_date, null::date) as condition_end_date
	,  coalesce(vd.visit_detail_end_date, null::date) as condition_end_datetime
	, case
		when coe.condition_type_source_value = 'P' then 44786627 -- Primary condition
		when coe.condition_type_source_value = 'S' then 44786629 -- Secondary condition
		when coe.condition_type_source_value = 'R' then 44786629 -- Secondary condition
		else 0 
		end as condition_type_concept_id
	, 0::integer as condition_status_concept_id -- TODO
	, null::text as stop_reason
	, 0::integer as provider_id
	, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
	, coalesce(vd.visit_detail_id, 0) as visit_detail_id
	, condition_source_value
	, 0::integer as condition_source_concept_id -- TODO
	, null::text as condition_status_source_value
	, coe.visit_detail_source_value
from etl.condition_occurrence_extract coe
inner join omop.visit_detail vd
on vd.visit_detail_source_value = coe.visit_detail_source_value
and vd.visit_detail_type_concept_id = 32034
;

-- 2.2 Transform
--
-- get condition_source_concept_id
--
----------------------------------------------------

drop table if exists etl.condition_occurrence_tr2;

create table if not exists etl.condition_occurrence_tr2
as
select 0 as	condition_occurrence_id
	, person_id
	, 0 as condition_concept_id  -- TODO
	, condition_start_date
	, condition_start_datetime
	, condition_end_date
	, condition_end_datetime
	, condition_type_concept_id
	, 0::integer as condition_status_concept_id -- TODO
	, stop_reason
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, condition_source_value
	, coalesce(c.concept_id, 0::integer) as condition_source_concept_id
	, null::text as condition_status_source_value
from etl.condition_occurrence_tr1 cotr1
left outer join omop.concept c
on cotr1.condition_source_value = c.concept_code
and c.vocabulary_id = 'Lille CIM10'
;

-- 2.3 Transform
--
-- get condition_concept_id
--
----------------------------------------------------

drop table if exists etl.condition_occurrence_tr3;

create table if not exists etl.condition_occurrence_tr3
as
select 0 as	condition_occurrence_id
	, person_id
	, coalesce(cr.concept_id_2, 0) as condition_concept_id
	, condition_start_date
	, condition_start_datetime
	, condition_end_date
	, condition_end_datetime
	, condition_type_concept_id
	, condition_status_concept_id -- TODO
	, stop_reason
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, condition_source_value
	, condition_source_concept_id
	, condition_status_source_value
from etl.condition_occurrence_tr2 cotr2
left outer join omop.concept_relationship cr
on cotr2.condition_source_concept_id = cr.concept_id_1
and cr.relationship_id = 'Lille CIM10 to ICD10'
;

-- 2.2 Transform
--
-- get condition_source_concept_id
--
----------------------------------------------------

drop table if exists etl.condition_occurrence_tr;

create table if not exists etl.condition_occurrence_tr
as
select 0 as	condition_occurrence_id
	, person_id
	, condition_concept_id  -- TODO
	, condition_start_date
	, condition_start_datetime
	, condition_end_date
	, condition_end_datetime
	, condition_type_concept_id
	, condition_status_concept_id -- TODO
	, stop_reason
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, condition_source_value
	, condition_source_concept_id
	, condition_status_source_value
from etl.condition_occurrence_tr3
;


-- 3. Load into omop_anesthesia.visit_occurrence
----------------------------------------------------

truncate omop.condition_occurrence;
alter sequence etl.condition_occurrence_id_seq restart with 1;

insert into  omop.condition_occurrence(
	condition_occurrence_id
	, person_id
	, condition_concept_id
	, condition_start_date
	, condition_start_datetime
	, condition_end_date
	, condition_end_datetime
	, condition_type_concept_id
	, condition_status_concept_id
	, stop_reason
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, condition_source_value
	, condition_source_concept_id
	, condition_status_source_value
)
-----
select nextval('etl.condition_occurrence_id_seq')
	, person_id
	, condition_concept_id
	, condition_start_date
	, condition_start_datetime
	, condition_end_date
	, condition_end_datetime
	, condition_type_concept_id
	, condition_status_concept_id
	, stop_reason
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, condition_source_value
	, condition_source_concept_id
	, condition_status_source_value
from etl.condition_occurrence_tr;