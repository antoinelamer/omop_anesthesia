----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 2020-03-28
--
-- 1) Extract start and end date of episode
-- 2) Transform and mapp to OHDSI concept_id
-- 3) Load into omop.episode
--
----------------------------------------


-- 1. Extract 
------------------------------------------------------------

-- V1

drop table if exists etl.episode_event_extract;

create table if not exists etl.episode_event_extract as 
--
-- Operating room Episode
--
select 
		0::integer as episode_id
		, 4021813 as episode_concept_id
		, coalesce(p.person_id, 0) as person_id
		, date_debut_bloc as episode_start_datetime
		, date_fin_bloc as episode_end_datetime
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 1::integer as episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, id_patient::text as person_source_value
		, id_intervention::text as visit_detail_source_value
from raw_data.intervention_patient ip
-- join with person
left outer join omop.person p
on ip.id_patient::text = p.person_source_value
-- join with visit_detail
left outer join omop.visit_detail vd
on ip.id_intervention::text = vd.visit_detail_source_value
where date_debut_bloc is not null and  date_fin_bloc is not null
--
-- SSPI Episode
--
union
select 
		0::integer as episode_id
		, 4134563 as episode_concept_id
		, coalesce(p.person_id, 0) as person_id
		, date_debut_sspi as episode_start_datetime
		, date_fin_sspi as episode_end_datetime
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 1::integer as episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, id_patient::text as person_source_value
		, id_intervention::text as visit_detail_source_value
from raw_data.intervention_patient ip
left outer join omop.person p
on ip.id_patient::text = p.person_source_value
left outer join omop.visit_detail vd
on ip.id_intervention::text = vd.visit_detail_source_value
where date_debut_sspi is not null and  date_fin_sspi is not null
;

--

select 	0::integer as episode_id
		, 4134563 as episode_concept_id
		, coalesce(p.person_id, 0) as person_id
		, date_debut_sspi as episode_start_datetime
		, date_fin_sspi as episode_end_datetime
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 1::integer as episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, id_patient::text as person_source_value
		, id_intervention::text as visit_detail_source_value
from raw_data.interv_fenetre_etude
left outer join omop.visit_detail vd
on ip.id_intervention::text = vd.visit_detail_source_value

-- V2
-----------------------------------------------------------------------

-- 1) Extract relevant data from fenetre_etude
-- 
-------------------------------------------------------------------------------

drop table if exists etl.episode_extract;

create table if not exists etl.episode_extract as
select  date_debut_fenetre as episode_start_datetime
		, date_fin_fenetre as episode_end_datetime
		, num_fenetre as episode_number
		, id_intervention::text as visit_detail_source_value
		, id_fenetre_etude::text episode_source_value
		, (select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit') as visit_detail_concept_id
from raw_data.interv_fenetre_etude;

-- 2)
-- Mapping with person_id, visit_detail_id, visit_occurrence_id
-------------------------------------------------------------------------------

drop table if exists etl.episode_tr1;

create table if not exists etl.episode_tr1 as
select 0::integer as episode_id
		, 0::integer episode_concept_id
		, coalesce(vd.person_id, 0) as person_id
		, ee.episode_start_datetime
		, ee.episode_end_datetime
		, 0::integer as episode_parent_id
		, 32544 as episode_type_concept_id -- Episode defined in EHR
		, 0::integer as episode_object_concept_id
		, ee.episode_number
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, ee.visit_detail_source_value
		, ee.episode_source_value
		, coalesce(c.concept_id, 0) as episode_source_concept_id
from etl.episode_extract ee
-- visit_detail_id
inner join omop.visit_detail vd
on ee.visit_detail_source_value = vd.visit_detail_source_value
and ee.visit_detail_concept_id = vd.visit_detail_concept_id
-- episode_source_concept_id
left outer join omop.concept c
on ee.episode_source_value = c.concept_code
and c.vocabulary_id = 'Lille Episode';

-- Mapping with episode_concept_id
----------------------------------------------




------------------------------------------------------------------

truncate omop.episode;
alter sequence etl.episode_id_seq restart with 1;

insert into omop.episode (
	episode_id
	, person_id
	, episode_concept_id       
	, episode_start_datetime   
	, episode_end_datetime     
	, episode_parent_id        
	, episode_number           
	, episode_object_concept_id
	, episode_type_concept_id  
	, visit_occurrence_id
	, visit_detail_id
	, episode_source_value     
	, episode_source_concept_id
	)
select nextval('etl.episode_id_seq')
	, person_id
	, episode_concept_id       
	, episode_start_datetime   
	, episode_end_datetime     
	, episode_parent_id        
	, episode_number           
	, episode_object_concept_id -- TODO
	, episode_type_concept_id  
	, visit_occurrence_id
	, visit_detail_id
	, episode_source_value     -- TODO 
	, episode_source_concept_id -- TODO
from etl.episode_tr1;

select * from omop.episode;
