------
--
--

select * from raw_data.interv_seuil limit 100;


drop table if exists etl.measurement_threshold_extract;

create table if not exists etl.measurement_threshold_extract as
select id_intervention::text as visit_detail_source_value
		, id_seuil::text as measurement_source_value
		, date_evt_declencheur_fen as measurement_datetime
		, temps_hors_seuil as value_as_number
		, case
		when b_temps_hors_seuil = 0 then 4188540 -- no
		when b_temps_hors_seuil = 1 then 4188539 -- yes
		else 4080894 -- null
		end as value_as_concept_id
		, temps_hors_seuil as value_source_value
from raw_data.interv_seuil;

--
-- join with visit_detail

drop table if exists etl.measurement_threshold_tr;

create table if not exists etl.measurement_threshold_tr as
select 0::integer as measurement_id
		, vd.person_id
		, 0::integer as measurement_concept_id
		, null::date as measurement_date
		, mte.measurement_datetime
		, null::varchar as measurement_time
		, 45754907::integer as measurement_type_concept_id -- derived value
		, null::integer as operator_concept_id
		, mte.value_as_number
		, mte.value_as_concept_id
		, null::integer as unit_concept_id
		, null::numeric as range_low
		, null::numeric as range_high
		, 0 as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, mte.measurement_source_value as measurement_source_value
		, c.concept_id as measurement_source_concept_id -- TODO
		, null::varchar as unit_source_value
		, null::varchar as value_source_value
from etl.measurement_threshold_extract mte
-- visit_detail
left outer join omop.visit_detail vd
on mte.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 
	(select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit')
-- concept for measurement_source_concept_id
left outer join omop.concept c
on mte.measurement_source_value = c.concept_code
and c.vocabulary_id = 'Lille Threshold'
;

-- 3) Load into omop.measurement
----------------------------------------------------

delete from omop.measurement where measurement_source_concept_id 
	in (select concept_id from omop.concept where vocabulary_id = 'Lille Threshold')
	;

insert into omop.measurement (
		measurement_id
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
)
-----
select nextval('etl.measurement_id_seq')
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_threshold_tr
where person_id is not null;

