----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 2020-03-28
--
-- 1) Extract raw data
-- 2) Transform and map to standard concept_id
-- 3) Load into omop_anesthesia.visit_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-------------------------------------------------------------------------------
-- Source 2 : indicators 
-- ex : mean of heart rate during surgery, ...
-------------------------------------------------------------------------------

drop table if exists etl.measurement_indicator_extract;

create table if not exists etl.measurement_indicator_extract as
select id_intervention::text as visit_detail_source_value
		, id_indicateur_mesure::text as measurement_source_value
		, date_debut_fenetre as measurement_datetime
		, valeur as value_as_number
from raw_data.interv_indicateur;

-- 
------------------------------------------------------------

drop table if exists etl.measurement_indicator_tr;

create table if not exists etl.measurement_indicator_tr as
select 0::integer as measurement_id
		, coalesce(vd.person_id, 0) as person_id
		, 0::integer as measurement_concept_id
		, mie.measurement_datetime::date as measurement_date
		, mie.measurement_datetime
		, null::varchar as measurement_time
		, 45754907::integer as measurement_type_concept_id -- derived value
		, null::integer as operator_concept_id
		, mie.value_as_number
		, null::integer as value_as_concept_id
		, null::integer as unit_concept_id
		, null::numeric as range_low
		, null::numeric as range_high
		, 0 as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, mie.measurement_source_value
		, c.concept_id as measurement_source_concept_id
		, null::varchar as unit_source_value
		, null::varchar as value_source_value
from etl.measurement_indicator_extract mie
-- visit_detail_id
left outer join omop.visit_detail vd
on mie.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 
	(select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit')
--
left outer join omop.concept c
on mie.measurement_source_value = c.concept_code
and c.vocabulary_id = 'Lille Indicator'
;

-- 3) Load into omop.measurement
----------------------------------------------------

delete from omop.measurement where measurement_source_concept_id 
	in (select concept_id from omop.concept where vocabulary_id = 'Lille Indicator')
	;

insert into omop.measurement (
		measurement_id
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
)
-----
select nextval('etl.measurement_id_seq')
		, person_id
		, measurement_concept_id
		, measurement_date
		, measurement_datetime
		, measurement_time
		, measurement_type_concept_id
		, operator_concept_id
		, value_as_number
		, value_as_concept_id
		, unit_concept_id
		, range_low
		, range_high
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, measurement_source_value
		, measurement_source_concept_id
		, unit_source_value
		, value_source_value
from etl.measurement_indicator_tr
where person_id is not null
;