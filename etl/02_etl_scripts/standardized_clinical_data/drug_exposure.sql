----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 03/12/19
--
-- 1. Extract data from the source table
-- 2. Transform and mapp to OHDSI concept_id
-- 3. Load into omop_anesthesia.visit_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/


-- 1. Extract measurement value, parameter, unit, date and 
-- operation identifier from the source table
------------------------------------------------------------

drop table if exists etl.drug_exposure_extract;

create table if not exists etl.drug_exposure_extract as 
--
select id_intervention::text as visit_detail_source_value
		, id_evenement::text as drug_source_value
		, posologie as quantity
		, date_occurence as drug_exposure_start_datetime
		, coalesce(date_fin, date_occurence) as drug_exposure_end_datetime
		, voie_injection as route_source_value
		, id_unite_posologie::text as dose_unit_source_value
from raw_data.interv_medicament;

select distinct voie_injection from raw_data.interv_medicament;

-- 2. Transform
--
-- Mapping with concept_id
-- 
-- person_id
-- visit_occurrence_id
-- visit_detail_id
--
-- drug_concept_id
-- drug_type_concept_id
-- unit_concept_id
-- 
----------------------------------------------------

-- left outer join with visit_detail
-- to get person_id, visit_occurrence_id, visit_detail_id

drop table if exists etl.drug_exposure_tr_1;

create table if not exists etl.drug_exposure_tr_1 as 
select 	0 as drug_exposure_id
		, coalesce(vd.person_id, 0) as person_id
		, --coalesce(cm.concept_id, 0) 
		0::integer as drug_concept_id
		, dee.drug_exposure_start_datetime::date as drug_exposure_start_date
		, dee.drug_exposure_start_datetime as drug_exposure_start_datetime
		, dee.drug_exposure_start_datetime::date as drug_exposure_end_date
		, dee.drug_exposure_end_datetime
		, null::date as verbatim_end_date
		, 0::integer as drug_type_concept_id  -- TODO
		, null::float as quantity
		, 0::integer as route_concept_id -- TODO
		, 0 as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, drug_source_value
		, 0::integer as drug_source_concept_id -- TODO
		, null::text as route_source_value
		, null::text as dose_unit_source_value
from etl.drug_exposure_extract dee
left outer join omop.visit_detail vd
on dee.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 2100000000 -- Operating theatre
;

-- join with concept to get drug_source_concept_id
-- drug_type_concept_id = 43542358

drop table if exists etl.drug_exposure_tr_2;

create table if not exists etl.drug_exposure_tr_2 as 
select 	0 as drug_exposure_id
		, det1.person_id
		, 0::integer as drug_concept_id
		, det1.drug_exposure_start_date
		, det1.drug_exposure_start_datetime
		, det1.drug_exposure_end_date
		, det1.drug_exposure_end_datetime
		, det1.verbatim_end_date
		, 43542358 as drug_type_concept_id  -- Physician administered drug (identified from EHR observation)
		, det1.quantity
		, 0::integer as route_concept_id -- TODO
		, det1.provider_id
		, det1.visit_occurrence_id
		, det1.visit_detail_id
		, drug_source_value
		, coalesce(cm.concept_id, 0) as drug_source_concept_id
		, null::text as route_source_value
		, null::text as dose_unit_source_value
from etl.drug_exposure_tr_1 det1
left outer join omop.concept cm
on det1.drug_source_value = cm.concept_code
and cm.vocabulary_id = 'Lille AIMS Drug'

select * from omop.concept where vocabulary_id = 'Lille AIMS Drug'

select c.concept_id, c.concept_name, count(*)
from etl.drug_exposure_tr_2 det2 left outer join omop.concept c
on det2.drug_source_concept_id = c.concept_id
group by concept_id, concept_name
order by count(*) desc


-- left outer join with concept_relationship
-- to get for drug_concept_id

drop table if exists etl.drug_exposure_tr;

create table if not exists etl.drug_exposure_tr as 
select 	0 as drug_exposure_id
		, det2.person_id
		, coalesce(cr.concept_id_2, 0) as drug_concept_id
		, det2.drug_exposure_start_date
		, det2.drug_exposure_start_datetime
		, det2.drug_exposure_end_date
		, det2.drug_exposure_end_datetime
		, det2.verbatim_end_date
		, det2.drug_type_concept_id
		, null::float as quantity
		, det2.route_concept_id
		, det2.provider_id
		, det2.visit_occurrence_id
		, det2.visit_detail_id
		, drug_source_value
		, det2.drug_source_concept_id
		, null::text as route_source_value
		, null::text as dose_unit_source_value
from etl.drug_exposure_tr_2 det2
left outer join omop.concept_relationship cr
on det2.drug_source_concept_id = cr.concept_id_1
and relationship_id = 'Lille Drug to SNOMED';


-- Verif

select c.concept_name, count(*)
from etl.drug_exposure_tr_3 det3 left outer join omop.concept c
on det3.drug_concept_id = c.concept_id
group by concept_name
order by count(*) desc;



-- 3. Load into omop_anesthesia.person
----------------------------------------------------


truncate omop.drug_exposure;
alter sequence etl.drug_exposure_id_seq restart with 1;

insert into omop.drug_exposure (
		drug_exposure_id
		, person_id
		, drug_concept_id
		, drug_exposure_start_date
		, drug_exposure_start_datetime
		, drug_exposure_end_date
		, drug_exposure_end_datetime
		, verbatim_end_date
		, drug_type_concept_id
		, quantity
		, route_concept_id
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, drug_source_value
		, drug_source_concept_id
		, route_source_value
		, dose_unit_source_value
)
-----
select nextval('etl.drug_exposure_id_seq')
		, person_id
		, drug_concept_id
		, drug_exposure_start_date
		, drug_exposure_start_datetime
		, drug_exposure_end_date
		, drug_exposure_end_datetime
		, verbatim_end_date
		, drug_type_concept_id
		, quantity
		, route_concept_id
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, drug_source_value
		, drug_source_concept_id
		, route_source_value
		, dose_unit_source_value
from etl.drug_exposure_tr
;




select c.concept_name, count(*)
from omop.drug_exposure de
left outer join omop.concept c
on de.drug_source_concept_id = c.concept_id
group by c.concept_name
order by count(*) desc;

select * from omop.concept where concept_id = 2000003340;





