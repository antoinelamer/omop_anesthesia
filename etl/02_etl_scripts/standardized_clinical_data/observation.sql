----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 03/12/19
--
-- 1.1) Extract age, weight, height, bmi, asa from the source table
-- 1.2) Extract death from visit_detail (discharge mode)
-- 2) Transform and mapp to OHDSI concept_id
-- 3) Load into omop.observation
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/


-- 1) Extract age, weight, height, bmi, asa from the source table
------------------------------------------------------------

drop table if exists etl.observation_extract_demo;

create table if not exists etl.observation_extract_demo as 
--
-- Age
--
select 4265453 as observation_concept_id
		, id_intervention::text as visit_detail_source_value
		, date_heure_debut_intervention as observation_datetime
		, age::float as value_as_number
		, null::integer as value_as_concept_id
		, 'âge' as observation_source_value
		, 'ans'::text as unit_source_value
from raw_data.intervention_patient
where age is not null
--
-- Weight
--
union 
select 37111521 as observation_concept_id
		, id_intervention::text
		, date_heure_debut_intervention as observation_datetime
		, poids::float as value_as_number
		, null::integer as value_as_concept_id
		, 'poids' as observation_source_value
		, 'kgs'::text as unit_source_value
from raw_data.intervention_patient
where poids is not null
--
-- Ideal Body Weight
--
union 
select 4062985 as observation_concept_id
		, id_intervention::text
		, date_heure_debut_intervention as observation_datetime
		, poids_ideal::float as value_as_number
		, null::integer as value_as_concept_id
		, 'poids idéal' as observation_source_value
		, 'kgs'::text as unit_source_value
from raw_data.intervention_patient
where poids is not null
--
-- Taille
--
union 
select 0 as observation_concept_id -- TODO
		, id_intervention::text
		, date_heure_debut_intervention as observation_datetime
		, taille::float as value_as_number
		, null::integer as value_as_concept_id
		, 'taille' as observation_source_value
		, 'cms'::text as unit_source_value
from raw_data.intervention_patient
where taille is not null
--
-- IMC
--
union 
select 4245997 as observation_concept_id
		, id_intervention::text
		, date_heure_debut_intervention as observation_datetime
		, imc::float as value_as_number
		, null::integer as value_as_concept_id
		, 'imc' as observation_source_value
		, 'kg/cm²'::text as unit_source_value
from raw_data.intervention_patient
where imc is not null
--
-- ASA
--
union 
select 
		4185946 as observation_concept_id
		,id_intervention::text
		, date_heure_debut_intervention as observation_datetime
		, asa_nm::float as value_as_number
		, case 
			when asa_nm = 1 then 4186042
			when asa_nm = 2 then 4184967
			when asa_nm = 3 then 4186043
			when asa_nm = 4 then 4211334
			when asa_nm = 5 then 4186044
			when asa_nm = 6 then 4186045
			else 0::integer
		end as value_as_concept_id
		, 'ASA' as observation_source_value
		, null::text as unit_source_value
from raw_data.intervention_patient
where asa_nm is not null
--
-- Urgent
--
union 
select 
		4156188 as observation_concept_id
		,id_intervention::text
		, date_heure_debut_intervention as observation_datetime
		, null::float as value_as_number
		, case 
		when urgence = 1 then 4014167 -- urgent
		else 4216169 -- Normal priority
		end as value_as_concept_id
		, urgence::text as observation_source_value
		, null::text as unit_source_value
from raw_data.intervention_patient
where urgence is not null
;

-- Urgent 4014167

-- 2. Transform
--
-- Mapping with concept_id
-- 
-- person_id
-- visit_detail_id
-- unit_concept_id
-- unit_concept_id
-- ..
--
-- Value, keep only numeric value ?
-- 
----------------------------------------------------

-- 

drop table if exists etl.observation_tr_demo;

create table if not exists etl.observation_tr_demo as 
select 	0 as observation_id
		, coalesce(vd.person_id, 0) as person_id
		, observation_concept_id
		, observation_datetime::date as observation_date
		, observation_datetime
		, 38000280::integer as observation_type_concept_id  -- TODO
		, value_as_number -- keep only numeric value
		, null::text as value_as_string
		, value_as_concept_id
		, null::integer as qualifier_concept_id
		, case -- todo join with concept and concept_relationship
			when unit_source_value = 'ans' then  9448
			when unit_source_value = 'kgs' then 9529
			when unit_source_value = 'cms' then 8582
			when unit_source_value = 'km/m²' then 9531
			else null::integer 
		end as unit_concept_id
		, null::integer as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, observation_source_value
		, 0::integer as observation_source_concept_id -- TODO
		, unit_source_value
		, null::text as qualifier_source_value
		, null::integer as observation_event_id
		, 0::integer as obs_event_field_concept_id
		, null::timestamp as value_as_datetime
from etl.observation_extract_demo oe
left outer join omop.visit_detail vd
on oe.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 
	(select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit');

----------------------------------------------------------------------
-- Steps of procedure
----------------------------------------------------------------------

drop table if exists etl.observation_extract_steps;

create table if not exists etl.observation_extract_steps as
--
select id_intervention::text as visit_detail_source_value
		, 40483148 as observation_concept_id
		, date_debut_bloc as observation_datetime
		, 'Entrée bloc' as observation_source_value
from raw_data.intervention_patient
where date_debut_bloc is not null
union
select id_intervention::text as visit_detail_source_value
		, 40481364 as observation_concept_id
		, date_fin_bloc as observation_datetime
		, 'Sortie bloc' as observation_source_value
from raw_data.intervention_patient
where date_fin_bloc is not null
union
select id_intervention::text as visit_detail_source_value
		, 40482694 as observation_concept_id
		, date_debut_sspi as observation_datetime
		, 'Entrée SSPI' as observation_source_value
from raw_data.intervention_patient
where date_debut_sspi is not null
union
select id_intervention::text as visit_detail_source_value
		, 40483530 as observation_concept_id
		, date_fin_sspi as observation_datetime
		, 'Sortie SSPI' as observation_source_value
from raw_data.intervention_patient
where date_fin_sspi is not null
;

--

drop table if exists etl.observation_tr_steps;

create table if not exists etl.observation_tr_steps as 
select 	0::integer as observation_id
		, coalesce(vd.person_id, 0) as person_id
		, observation_concept_id
		, observation_datetime::date as observation_date
		, observation_datetime
		, 38000280 as observation_type_concept_id  -- Observation recorded from EHR
		, null::integer value_as_number -- keep only numeric value
		, null::text as value_as_string
		, null::float value_as_concept_id
		, null::integer as qualifier_concept_id
		, 0::integer as unit_concept_id
		, null::integer as provider_id
		, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
		, coalesce(vd.visit_detail_id, 0) as visit_detail_id
		, observation_source_value
		, 0::integer as observation_source_concept_id -- TODO
		, 0::text unit_source_value
		, null::text as qualifier_source_value
		, null::integer as observation_event_id
		, 0::integer as obs_event_field_concept_id -- TODO
		, null::timestamp as value_as_datetime
from etl.observation_extract_steps oe
left outer join omop.visit_detail vd
on oe.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 
	(select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit');

----------------------------------------------------------------------
--- Death (from visit_detail and visit_occurrence)
----------------------------------------------------------------------

drop table if exists etl.observation_death;

create table if not exists etl.observation_death as
select 
		0::integer as observation_id
		, person_id
		, 4306655 as observation_concept_id 	   -- death
		, death_datetime as observation_date
		, death_datetime as observation_datetime
		, 32494 as observation_type_concept_id			-- EHR record patient status "Deceased"
		, null::integer value_as_number
		, null::text as value_as_string
		, null::float value_as_concept_id
		, null::integer as qualifier_concept_id
		, null::integer as unit_concept_id
		, null::integer as provider_id
		, visit_occurrence_id
		, visit_detail_id 						-- todo
		, null::text as observation_source_value
		, 0 as observation_source_concept_id 		-- todo
		, null::text  as unit_source_value
		, null::text qualifier_source_value
		, 0::integer as observation_event_id
		, 0 as obs_event_field_concept_id 			-- todo
		, null::timestamp as value_as_datetime
from etl.person_death
;

-- 3. Load into omop_anesthesia.person
----------------------------------------------------

truncate omop.observation;
alter sequence etl.observation_id_seq restart with 1;

insert into omop.observation (
		observation_id
		, person_id
		, observation_concept_id
		, observation_date
		, observation_datetime
		, observation_type_concept_id
		, value_as_number
		, value_as_string
		, value_as_concept_id
		, qualifier_concept_id
		, unit_concept_id
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, observation_source_value
		, observation_source_concept_id
		, unit_source_value
		, qualifier_source_value
		, observation_event_id
		, obs_event_field_concept_id
		, value_as_datetime
)
--
-- Demographics
--
select nextval('etl.observation_id_seq')
		, person_id
		, observation_concept_id
		, observation_date
		, observation_datetime
		, observation_type_concept_id
		, value_as_number
		, value_as_string
		, value_as_concept_id
		, qualifier_concept_id
		, unit_concept_id
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, observation_source_value
		, observation_source_concept_id
		, unit_source_value
		, qualifier_source_value
		, observation_event_id
		, obs_event_field_concept_id
		, value_as_datetime
from etl.observation_tr_demo
--
-- Steps
--
union
select nextval('etl.observation_id_seq')
		, person_id
		, observation_concept_id
		, observation_date
		, observation_datetime
		, observation_type_concept_id
		, value_as_number
		, value_as_string
		, value_as_concept_id
		, qualifier_concept_id
		, unit_concept_id
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, observation_source_value
		, observation_source_concept_id
		, unit_source_value
		, qualifier_source_value
		, observation_event_id
		, obs_event_field_concept_id
		, value_as_datetime
from etl.observation_tr_steps
--
-- union
--
union
select nextval('etl.observation_id_seq')
		, person_id
		, observation_concept_id
		, observation_date
		, observation_datetime
		, observation_type_concept_id
		, value_as_number
		, value_as_string
		, value_as_concept_id
		, qualifier_concept_id
		, unit_concept_id
		, provider_id
		, visit_occurrence_id
		, visit_detail_id
		, observation_source_value
		, observation_source_concept_id
		, unit_source_value
		, qualifier_source_value
		, observation_event_id
		, obs_event_field_concept_id
		, value_as_datetime
from etl.observation_death
;

--------------------------------------------------------------------

-- select * from omop.observation where observation_concept_id = 4156188 limit 100;

