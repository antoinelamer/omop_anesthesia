----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Update death_datetime in person
-- 
-- Antoine Lamer
--
-- 03/12/19
--
----------------------------------------

drop table if exists etl.person_death;

create table if not exists etl.person_death as
---
select vo.person_id
, vo.visit_occurrence_id
, vo.visit_end_datetime as death_datetime
from omop.visit_detail vd
inner join omop.visit_occurrence vo
on vd.visit_occurrence_id = vo.visit_occurrence_id
where vd.discharge_to_source_value = '9'
group by vo.person_id, vo.visit_occurrence_id, vo.visit_end_datetime
;

--
-- Update Person
--

update omop.person
set death_datetime = null;

update omop.person p
set death_datetime = pd.death_datetime
from etl.person_death pd
where p.person_id = pd.person_id;

--
-- 
--


