----------------------------------------
--
-- ETL 
--
-- Table : VISIT_OCCURRENCE
--
-- Antoine Lamer
--
-- 2020-03-27
--
-- 1) Extract patient_id, birth_date, sex from the source table
-- 2) Transform and mapp to OHDSI concept_id
-- 3) Load into omop_anesthesia.visit_occurrence
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- 1) Extract visit_start_date, visit_end_date, visit_source_value,
-- admitted_from_source_value, discharge_to_source_value
-- from the source table
------------------------------------------------------------

drop table if exists etl.visit_occurrence_extract;

create table if not exists etl.visit_occurrence_extract
as
-- Raw data
select 
		trim(iep)::text as visit_source_value
		, id_patient::text as person_source_value
		, date_debut_sejour as visit_start_date
		--, date_debut_sejour as visit_start_datetime
		, date_fin_sejour as visit_end_date
		--, date_fin_sejour as visit_end_datetime
		--, code_mode_entree as admitted_from_source_value
		--, code_mode_sortie as discharge_to_source_value
from raw_data.sejour -- 1 row per operation
where iep is not null;

-- 2. Transform
--
-- visit_type_concept_id
-- care_site_id
-- visit_source_concept_id
-- admitted_from_concept_id
-- discharge_to_concept_id
--
----------------------------------------------------

drop table if exists etl.visit_occurrence_tr;

create table if not exists etl.visit_occurrence_tr
as
select 0 as	visit_occurrence_id
	, coalesce(p.person_id, 0) as person_id
	, 9201 as visit_concept_id -- update with emergency visit
	, visit_start_date
	, visit_start_date as visit_start_datetime
	, visit_end_date
	, visit_end_date as visit_end_datetime
	, 32034 as visit_type_concept_id
	, 0 as provider_id
	, coalesce((select min(care_site_id) from omop.care_site where care_site_name = 'CHU Lille'), 0) as care_site_id -- update with Lille
	, visit_source_value
	, 0 as visit_source_concept_id
	, 0 as admitted_from_concept_id -- to do
	, null::text as admitted_from_source_value
	, 0 as discharge_to_concept_id -- to do
	, null::text as discharge_to_source_value
	, 0 as preceding_visit_occurrence_id
	, voe.person_source_value
from etl.visit_occurrence_extract  voe
inner join omop.person p
on voe.person_source_value = p.person_source_value
where visit_start_date is not null
;

-- 3. Load into omop.visit_occurrence
----------------------------------------------------

truncate omop.visit_occurrence;
alter sequence etl.visit_occurrence_id_seq restart with 1;

insert into omop.visit_occurrence (
	visit_occurrence_id
	, person_id
	, visit_concept_id
	, visit_start_date
	, visit_start_datetime
	, visit_end_date
	, visit_end_datetime
	, visit_type_concept_id
	, provider_id
	, care_site_id
	, visit_source_value
	, visit_source_concept_id
	, admitted_from_concept_id
	, admitted_from_source_value
	, discharge_to_concept_id
	, discharge_to_source_value
	, preceding_visit_occurrence_id
)
-----
select nextval('etl.visit_occurrence_id_seq')
	, person_id
	, visit_concept_id
	, visit_start_date
	, visit_start_date as visit_start_datetime
	, visit_end_date
	, visit_end_datetime
	, visit_type_concept_id
	, provider_id
	, care_site_id
	, visit_source_value
	, visit_source_concept_id
	, admitted_from_concept_id
	, admitted_from_source_value
	, discharge_to_concept_id
	, discharge_to_source_value
	, preceding_visit_occurrence_id
from etl.visit_occurrence_tr;

select * from omop.visit_occurrence where length(visit_source_value) > 8
select * from raw_data.sejour where length(iep) > 8
