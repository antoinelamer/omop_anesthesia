----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Antoine Lamer
--
-- 03/12/19
--
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- Person
----------------------------------------------------

-- Extract patient_id, birth_date, sex
----------------------------------------------------

drop table etl.note_extract;

create table etl.note_extract
as
-- Raw data
select id_intervention::text as visit_detail_source_value
		, id_source::text as note_title
		, lib_info_primaire as raw_note_text_1
		, lib_info_primaire_libre as note_text_2
		, replace(lib_info_primaire, 'Information primaire inconnue', '') as note_text_1
from raw_data.interv_preop -- 1 row per operation
--limit 100
;

select *
from raw_data.interv_preop -- 1 row per operation
limit 100;

-- Mapping with concept_id
----------------------------------------------------

drop table if exists etl.note_tr;

create table if not exists etl.note_tr
as
select 
  	0::integer as note_id
	, vd.person_id
	, 0::integer as note_event_id
	, 0::integer as note_event_field_concept_id
	, null::date as note_date -- set consultation date
	, vd.visit_detail_start_date::timestamp as note_datetime -- update with consultation date
	, 44814645::integer as note_type_concept_id -- Note
	, 46236104::integer as note_class_concept_id -- 
	, note_title
	, case
	when note_text_2 is null
	then note_text_1
	when note_text_1 is null
	then note_text_2
	else note_text_1 || '-' || note_text_2
	end as note_text
	, 32678::integer as encoding_concept_id -- UTF-8
	, 1585439::integer language_concept_id -- French
	, 0::integer provider_id -- todo
	, vd.visit_occurrence_id
	, vd.visit_detail_id
	, 'Consultation DIANE' as note_source_value
from etl.note_extract ne
inner join omop.visit_detail vd
on ne.visit_detail_source_value = vd.visit_detail_source_value
and vd.visit_detail_concept_id = 2100000000;

-- Load into omop_anesthesia.person
----------------------------------------------------

truncate omop.note;
alter sequence etl.note_id_seq restart with 1;

insert into omop.note (
  	note_id
	, person_id
	, note_event_id
	, note_event_field_concept_id
	, note_date
	, note_datetime
	, note_type_concept_id
	, note_class_concept_id
	, note_title
	, note_text
	, encoding_concept_id
	, language_concept_id
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, note_source_value	
	)
---
select nextval('etl.note_id_seq')
	, person_id
	, note_event_id
	, note_event_field_concept_id
	, note_date
	, note_datetime
	, note_type_concept_id
	, note_class_concept_id
	, note_title
	, note_text
	, encoding_concept_id
	, language_concept_id
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, note_source_value	
from etl.note_tr;

select * from omop.note;
