----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Table : VISIT_DETAIL
--
-- Antoine Lamer
--
-- 14/12/19
--
-- 2 sources :
-- operating room (AIMS)
-- unit stay (PMSI)
----------------------------------------

/*
Schemas:
raw_data = schema with raw data
etl = schema for etl transformations
omop = final OMOP schema
*/

-- 1) Operating room
-------------------------------------------------------------------------------------------------------

-- 1.1) Extract visit detail from the operating room

drop table if exists etl.visit_detail_extract_or;

create table if not exists etl.visit_detail_extract_or
as
select id_patient::text as person_source_value
		, id_intervention as visit_detail_source_value
		, iep as visit_source_value
		, coalesce(date_debut_bloc, date_debut_sspi) as visit_detail_start_datetime
		, coalesce(date_fin_sspi, date_fin_bloc) as visit_detail_end_datetime
		, salle as care_site_name
		, salle_nm::character as care_site_source_value
from raw_data.intervention_patient
where (date_debut_bloc is not null or date_debut_sspi is not null) 
and (date_fin_bloc is not null or date_fin_sspi is not null);

-- select * from raw_data.intervention_patient where id_intervention = 217657
-- sans date de bloc

select * from etl.visit_detail_extract_or where visit_detail_source_value = '217657'

-- 1.2) Get the foreign keys
--
-- person_id
-- visit_type_concept_id
-- care_site_id
-- visit_source_concept_id
-- admitted_from_concept_id
-- discharge_to_concept_id
--
----------------------------------------------------

drop table if exists etl.visit_detail_tr_or;

create table if not exists etl.visit_detail_tr_or
as
select 0 as	visit_detail_id
	, p.person_id
	, (select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit') as visit_detail_concept_id
	, visit_detail_start_datetime::date as visit_detail_start_date
	, visit_detail_start_datetime
	, visit_detail_end_datetime::date as visit_detail_end_date
	, visit_detail_end_datetime
	, 44818518 as visit_detail_type_concept_id
	, 0 as provider_id
	, coalesce(cs.care_site_id, 0) as care_site_id
	, visit_detail_source_value
	, 0 as visit_detail_source_concept_id
	, 0 as admitted_from_concept_id
	, 0::character as admitted_from_source_value
	, 0 as discharge_to_concept_id
	, 0::character as discharge_to_source_value
	, 0 as preceding_visit_detail_id
	, 0 as visit_detail_parent_id
	, coalesce(v.visit_occurrence_id, 0) as visit_occurrence_id
from etl.visit_detail_extract_or vde
-- Join for person_id
left outer join omop.person p
on p.person_source_value = vde.person_source_value
-- join for visit_occurrence_id
left outer join omop.visit_occurrence v
on v.visit_source_value = vde.visit_source_value
-- join for care_site_id
left outer join omop.care_site cs
on cs.care_site_source_value = vde.care_site_source_value
and cs.care_site_name = vde.care_site_name
;

-- 2) PMSI
--------------------------------------------------------------------------------------------

-- 1.1) Extract visit detail from the PMSI

drop table if exists etl.visit_detail_extract_pmsi;

create table if not exists etl.visit_detail_extract_pmsi as
select
	0::integer as person_source_value
	, id_mouvement as visit_detail_source_value
	, trim(iep) as visit_source_value
	, date_debut_mouvement as visit_detail_start_datetime
	, date_fin_mouvement as visit_detail_end_datetime
	--, lib_uf as care_site_name
	, code_uf as care_site_source_value
	, code_mode_entree as admitted_from_source_value
	, code_mode_sortie as discharge_to_source_value
from raw_data.mouvement;

-- 2.2) Get the foreign keys
--
-- Person_id
-- Visit_occurrence_id
-- admitted_from_concept_id
-- discharge_to_concept_id	
-- Care_site_id

-- # todo
-- admitted_from_concept_id
-- discharge_to_concept_id

drop table if exists etl.visit_detail_tr_pmsi;

create table if not exists etl.visit_detail_tr_pmsi
as
select 0 as	visit_detail_id
	, v.person_id
	, 0 as visit_detail_concept_id -- to_do
	, visit_detail_start_datetime::date as visit_detail_start_date
	, visit_detail_start_datetime
	, coalesce(visit_detail_end_datetime::date, visit_detail_start_datetime::date) as visit_detail_end_date
	, coalesce(visit_detail_end_datetime, visit_detail_start_datetime) as visit_detail_end_datetime
	, 32034 as visit_detail_type_concept_id -- Visit derived from EHR billing recordd
	, 0 as provider_id
	, coalesce(cs.care_site_id, 0) as care_site_id
	, visit_detail_source_value
	, 0 as visit_detail_source_concept_id
	, 0 as admitted_from_concept_id
	, vde.admitted_from_source_value
	, 0 as discharge_to_concept_id
	, vde.discharge_to_source_value
	, 0 as preceding_visit_detail_id
	, 0 as visit_detail_parent_id
	, coalesce(v.visit_occurrence_id, 0) as visit_occurrence_id
from etl.visit_detail_extract_pmsi vde
-- join for visit_occurrence_id and person_id
inner join omop.visit_occurrence v
on v.visit_source_value = vde.visit_source_value
-- join for care_site_id
left outer join omop.care_site cs
on cs.care_site_source_value = vde.care_site_source_value
--and cs.care_site_name = vde.care_site_name
;

-- 3) Load into omop.visit_detail
----------------------------------------------------

truncate omop.visit_detail;
alter sequence etl.visit_detail_id_seq restart with 1;

insert into omop.visit_detail (
	visit_detail_id
	, person_id
	, visit_detail_concept_id
	, visit_detail_start_date
	, visit_detail_start_datetime
	, visit_detail_end_date
	, visit_detail_end_datetime
	, visit_detail_type_concept_id
	, provider_id
	, care_site_id
	, visit_detail_source_value
	, visit_detail_source_concept_id
	, admitted_from_concept_id
	, admitted_from_source_value
	, discharge_to_concept_id
	, discharge_to_source_value
	, preceding_visit_detail_id
	, visit_detail_parent_id
	, visit_occurrence_id
)
--
-- OR
--
select nextval('etl.visit_detail_id_seq')
	, person_id
	, visit_detail_concept_id
	, visit_detail_start_date
	, visit_detail_start_datetime
	, visit_detail_end_date
	, visit_detail_end_datetime
	, visit_detail_type_concept_id
	, provider_id
	, care_site_id
	, visit_detail_source_value
	, visit_detail_source_concept_id
	, admitted_from_concept_id
	, admitted_from_source_value
	, discharge_to_concept_id
	, discharge_to_source_value
	, preceding_visit_detail_id
	, visit_detail_parent_id
	, visit_occurrence_id
from etl.visit_detail_tr_or
--
-- PMSI
-- 
union 
select nextval('etl.visit_detail_id_seq')
	, person_id
	, visit_detail_concept_id
	, visit_detail_start_date
	, visit_detail_start_datetime
	, visit_detail_end_date
	, visit_detail_end_datetime
	, visit_detail_type_concept_id
	, provider_id
	, care_site_id
	, visit_detail_source_value
	, visit_detail_source_concept_id
	, admitted_from_concept_id
	, admitted_from_source_value
	, discharge_to_concept_id
	, discharge_to_source_value
	, preceding_visit_detail_id
	, visit_detail_parent_id
	, visit_occurrence_id
from etl.visit_detail_tr_pmsi
;