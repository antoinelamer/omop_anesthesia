----------------------------------------
--
-- ETL OMOP ANESTHESIE
--
-- Table : PROCEDURE_OCCURRENCE
--
-- Antoine Lamer
--
-- 14/12/19
--
-- 1. Extract patient_id
-- date of act
-- date of operating room
-- dates of post-anesthesia care unit
-- 2. Transform and mapp to OHDSI concept_id
-- 3. Load into omop.procedure_occurrence
--
----------------------------------------

-- 1. Extract procedure_concept_source_value, procedure_date
-- from
-- source 1 : administrative database
-- source 2 : AIMS
------------------------------------------------------------
drop table if exists etl.procedure_occurrence_extract;

create table if not exists etl.procedure_occurrence_extract
as
-- Raw data -- procedure from the administrative database
select 
		trim(iep) as visit_source_value
		, id_mouvement::text as visit_detail_source_value -- temporary field
		, code_acte_ccam as procedure_source_value
		, activite
		, phase
		, date_acte as procedure_date
		--, nombre_actes as quantity
from raw_data.acte
--group by id_mouvement, code_acte_ccam, activite, phase, nombre_actes
;

select * from raw_data.acte limit 100;

-- 2. Transform
--
----------------------------------------------------

-- 2.1
-- person_id, visit_occurrence_id, visit_detail_id

drop table if exists etl.procedure_occurrence_tr1;

create table if not exists etl.procedure_occurrence_tr1
as
select 0 as	procedure_occurrence_id
	, coalesce(vd.person_id, 0) as person_id
	, 0 as procedure_concept_id  -- TODO
	, poe.procedure_date
	, poe.procedure_date as procedure_datetime
	, 0::integer procedure_type_concept_id  -- TODO
	, 0::integer as modifier_concept_id  -- TODO
	, 1::integer as quantity
	, 0::integer as provider_id
	, coalesce(vd.visit_occurrence_id, 0) as visit_occurrence_id
	, coalesce(vd.visit_detail_id, 0) as visit_detail_id
	, procedure_source_value
	, 0::integer as procedure_source_concept_id -- TODO
	, null::character as modifier_source_value
from etl.procedure_occurrence_extract poe
inner join omop.visit_detail vd
on vd.visit_detail_source_value = poe.visit_detail_source_value
and vd.visit_detail_type_concept_id = 32034
;

-- 2.2
-- procedure_source_concept_id
-- procedure_type_concept_id

drop table if exists etl.procedure_occurrence_tr2;

create table if not exists etl.procedure_occurrence_tr2
as
select 0 as	procedure_occurrence_id
	, person_id
	, 0::integer as procedure_concept_id -- TODO
	, procedure_date
	, procedure_datetime
	, 44786630::integer procedure_type_concept_id -- Primary procedure
	, modifier_concept_id
	, quantity
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, procedure_source_value
	, coalesce(c.concept_id, 0) as procedure_source_concept_id
	, modifier_source_value
from etl.procedure_occurrence_tr1 potr1
left outer join omop.concept c
on potr1.procedure_source_value = c.concept_code
and c.vocabulary_id = 'Lille CCAM'
;

-- 2.3
-- procedure_concept_id

drop table if exists etl.procedure_occurrence_tr;

create table if not exists etl.procedure_occurrence_tr
as
select *
from etl.procedure_occurrence_tr2;

-- 3. Load into omop_anesthesia.visit_occurrence
----------------------------------------------------

truncate omop.procedure_occurrence;
alter sequence etl.procedure_occurrence_id_seq restart with 1;

insert into  omop.procedure_occurrence(
	procedure_occurrence_id
	, person_id
	, procedure_concept_id
	, procedure_date
	, procedure_datetime
	, procedure_type_concept_id
	, modifier_concept_id
	, quantity
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, procedure_source_value
	, procedure_source_concept_id
	, modifier_source_value
)
-----
select nextval('etl.procedure_occurrence_id_seq')
	, person_id
	, procedure_concept_id
	, procedure_date
	, procedure_datetime
	, procedure_type_concept_id
	, modifier_concept_id
	, quantity
	, provider_id
	, visit_occurrence_id
	, visit_detail_id
	, procedure_source_value
	, procedure_source_concept_id
	, modifier_source_value
from etl.procedure_occurrence_tr;