kklk

------------------------------------------------------------------------------------------------------------------
-- 1) concept_vocabulary_lille
------------------------------------------------------------------------------------------------------------------

-- Creation of concept_vocabulary_lille
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.concept_vocabulary_lille;

CREATE TABLE if not exists etl.concept_vocabulary_lille
 (
  concept_id			INTEGER  		NOT NULL,
  concept_name		    VARCHAR 	    NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 2) vocabulary_lille
------------------------------------------------------------------------------------------------------------------

-- Creation of vocabulary_lille
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.vocabulary_lille;

CREATE TABLE if not exists etl.vocabulary_lille 
(
  vocabulary_id				VARCHAR(20)		NOT NULL,
  vocabulary_name			VARCHAR(255)	NOT NULL,
  vocabulary_reference		VARCHAR(255)	NULL,
  vocabulary_version		VARCHAR(255)	NULL,
  vocabulary_concept_id		INTEGER			NOT NULL
)
;


------------------------------------------------------------------------------------------------------------------
-- 3) concept_relationship_lille
------------------------------------------------------------------------------------------------------------------

-- Creation of concept_relationship_lille relationship for Lille AIMS
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.concept_relationship_lille;

CREATE TABLE if not exists etl.concept_relationship_lille
 (
  concept_id			INTEGER  		,
  concept_name		    VARCHAR 	    NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 4) relationship_lille
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.relationship_lille;

CREATE TABLE if not exists etl.relationship_lille
(
  relationship_id 				character varying(20) NOT NULL,
  relationship_name 			character varying(255) NOT NULL,
  is_hierarchical 				character varying(1) NOT NULL,
  defines_ancestry 				character varying(1) NOT NULL,
  reverse_relationship_id 		character varying(20) NOT NULL,
  relationship_concept_id 		integer NOT NULL,
  CONSTRAINT xpk_relationship PRIMARY KEY (relationship_id)
);

------------------------------------------------------------------------------------------------------------------
-- 5) concept_lille
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.concept_lille;

CREATE TABLE if not exists etl.concept_lille
 (
  concept_id			INTEGER  		NOT NULL,
  concept_name		        VARCHAR 	        NOT NULL,
  domain_id			VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.1) aims_unit_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.concept_aims_unit_lille;

CREATE TABLE if not exists etl.concept_aims_unit_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.2) aims_measurement_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.aims_measurement_lille;

CREATE TABLE if not exists etl.aims_measurement_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.3) aims_drug_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.aims_drug_lille;

CREATE TABLE if not exists etl.aims_drug_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.4) aims_event_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.aims_event_lille;

CREATE TABLE if not exists etl.aims_event_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.5) aims_episode_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.aims_episode_lille;

CREATE TABLE if not exists etl.aims_episode_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.6) indicator_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.indicator_lille;

CREATE TABLE if not exists etl.indicator_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.7) threshold_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.threshold_lille;

CREATE TABLE if not exists etl.threshold_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.8) pmsi_ccam_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.pmsi_ccam_lille;

CREATE TABLE if not exists etl.pmsi_ccam_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 5.8) pmsi_cim10_lille.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.pmsi_cim10_lille;

CREATE TABLE if not exists etl.pmsi_cim10_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);


------------------------------------------------------------------------------------------------------------------
-- 5.9 new_concept_lille.csv
------------------------------------------------------------------------------------------------------------------


DROP TABLE if exists etl.new_concept_lille;

CREATE TABLE if not exists etl.new_concept_lille
 (
  concept_id			INTEGER  		NULL,
  concept_name		    VARCHAR			NOT NULL,
  domain_id				VARCHAR(20)		NOT NULL,
  vocabulary_id			VARCHAR(20)		NOT NULL,
  concept_class_id		VARCHAR(20)		NOT NULL,
  standard_concept		VARCHAR(1)		NULL,
  concept_code			VARCHAR(50)		NOT NULL,
  valid_start_date		DATE			NOT NULL,
  valid_end_date		DATE			NOT NULL,
  invalid_reason		VARCHAR(1)		NULL
);

------------------------------------------------------------------------------------------------------------------
-- 6) 6_aims_concept_relationship.csv
------------------------------------------------------------------------------------------------------------------

DROP TABLE if exists etl.prop_concept_relationship_lille;

CREATE TABLE if not exists etl.prop_concept_relationship_lille
 (
	 local_concept_code				VARCHAR(50)		NOT NULL
	 , local_concept_name				VARCHAR			NULL
	 , local_vocabulary_id				VARCHAR(20)		NOT NULL
	 , standard_concept_id				integer			NULL
	 , standard_concept_name			VARCHAR			NULL
	 , not_complete						VARCHAR			null
	 , relationship_id					VARCHAR(20)		NULL
	 , valid_start_date					DATE			NULL
	 , valid_end_date					DATE			NULL
	 , invalid_reason					VARCHAR(1)		NULL
	 );
	 
------------------------------------------------------------------------------------------------------------------
-- temp_concept_relationship.csv
------------------------------------------------------------------------------------------------------------------	 

drop table if exists etl.temp_concept_relationship;

create table if not exists etl.temp_concept_relationship (
	concept_id_1 integer NOT NULL
	, concept_id_2 integer NOT NULL
	, relationship_id character varying(20)
	, valid_start_date date NOT NULL
	, valid_end_date date NOT NULL
	, invalid_reason character varying(1)
);

