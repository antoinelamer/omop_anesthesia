------------------------------------------------------------------------------------------------------------------
-- Vocabulary loading for anesthesia procedure
--
-- Antoine Lamer
--
-- 25/01/2020
--
-- 1_concept_vocabulary_lille.csv -> concepts related to Lille vocabularies
-- 2_vocabulary_lille.csv -> Lille Vocabularies
-- 3_concept_relationship_lille.csv -> concepts related to Lille relationships
-- 4_relationship_lille.csv -> Lille relationships
-- 5_1_aims_unit_lille.csv -> units from AIMS
-- 5_2_aims_measurement_lille.csv -> measurements from AIMS
-- 5_3_aims_episode_lille.csv
------------------------------------------------------------------------------------------------------------------

-- Directory to change = '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies';
-- shema etl

------

delete from omop.concept where concept_id >= 2000002000;

delete from omop.concept_relationship t1 
where exists (select * from etl.temp_concept_relationship t2 where
			 t1.concept_id_1 = t2.concept_id_1
			 ); 

DELETE FROM omop.concept where concept_id in (select concept_id from etl.concept_vocabulary_lille);

delete from omop.concept where concept_id >= 2000000000;

select * from omop.domain order by 

------------------------------------------------------------------------------------------------------------------
-- 1) concept_vocabulary_lille
------------------------------------------------------------------------------------------------------------------

delete from etl.concept_vocabulary_lille;

COPY etl.concept_vocabulary_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/1_concept_vocabulary_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.concept_vocabulary_lille;

-- Insertion of concept_vocabulary_lille into concept
------------------------------------------------------------------------------------------------------------------

DELETE FROM omop.concept where concept_id in (select concept_id from etl.concept_vocabulary_lille);

INSERT INTO omop.concept
SELECT * FROM etl.concept_vocabulary_lille;

-- select * FROM omop.concept where concept_id in (select concept_id from etl.concept_vocabulary_lille);

------------------------------------------------------------------------------------------------------------------
-- 2) vocabulary_lille
------------------------------------------------------------------------------------------------------------------

-- insertion of csv file into vocabulary_lille
------------------------------------------------------------------------------------------------------------------

delete from etl.vocabulary_lille;

COPY etl.vocabulary_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/2_vocabulary_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.vocabulary_lille;

-- Insertion of vocabulary_lille into vocabulary
------------------------------------------------------------------------------------------------------------------
-- SELECT * FROM omop.vocabulary where vocabulary_id in (select vocabulary_id from etl.vocabulary_lille);

DELETE FROM omop.vocabulary where vocabulary_id in (select vocabulary_id from etl.vocabulary_lille);

INSERT INTO omop.vocabulary
SELECT * FROM etl.vocabulary_lille;

------------------------------------------------------------------------------------------------------------------
-- 3) concept_relationship_lille
------------------------------------------------------------------------------------------------------------------

-- Insertion of csv file into concept_relationship_lille
------------------------------------------------------------------------------------------------------------------

delete from etl.concept_relationship_lille;

COPY etl.concept_relationship_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/3_concept_relationship_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.concept_relationship_lille;

-- Insertion of concept_relationship_lille into concept
------------------------------------------------------------------------------------------------------------------

-- SELECT * FROM omop.concept where concept_id in (select concept_id from etl.concept_relationship_lille);

DELETE FROM omop.concept where concept_id in (select concept_id from etl.concept_relationship_lille);

INSERT INTO omop.concept
SELECT * FROM etl.concept_relationship_lille;

------------------------------------------------------------------------------------------------------------------
-- 4) relationship_lille
------------------------------------------------------------------------------------------------------------------

-- Insertion of csv file into relationship_lille
------------------------------------------------------------------------------------------------------------------

delete from etl.relationship_lille;

COPY etl.relationship_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/4_relationship_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.relationship_lille;

-- Copie de la table relationship_lille dans la table relationship
------------------------------------------------------------------------------------------------------------------

-- SELECT * FROM omop.relationship where relationship_id in (select relationship_id from etl.relationship_lille);

DELETE FROM omop.relationship where relationship_id in (select relationship_id from etl.relationship_lille);

INSERT INTO omop.relationship SELECT * FROM etl.relationship_lille;

------------------------------------------------------------------------------------------------------------------
-- 5) concept_lille
------------------------------------------------------------------------------------------------------------------

delete from etl.concept_lille;

DROP SEQUENCE if exists etl.concept_lille_id_seq;

CREATE SEQUENCE if not exists etl.concept_lille_id_seq INCREMENT 1 START 2000002000;

ALTER SEQUENCE etl.concept_lille_id_seq OWNER TO postgres;

------------------------------------------------------------------------------------------------------------------
-- 5.1) aims_unit_lille.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.concept_aims_unit_lille;

COPY etl.concept_aims_unit_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_1_aims_unit_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.concept_aims_unit_lille;

------------------------------------------------------------------------------------------------------------------

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.concept_aims_unit_lille s1;

-- select * from etl.concept_lille order by concept_id;

------------------------------------------------------------------------------------------------------------------
-- 5.2) aims_measurement_lille.csv
------------------------------------------------------------------------------------------------------------------

--

delete from etl.aims_measurement_lille;

COPY etl.aims_measurement_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_2_aims_measurement_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.aims_measurement_lille;

------------------------------------------------------------------------------------------------------------------

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.aims_measurement_lille s1;

-- select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.3) aims_drug_lille.csv
------------------------------------------------------------------------------------------------------------------

--

delete from etl.aims_drug_lille;

COPY etl.aims_drug_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_3_aims_drug_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.aims_drug_lille;

------------------------------------------------------------------------------------------------------------------

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.aims_drug_lille s1;

-- select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.4) aims_event_lille.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.aims_event_lille;

COPY etl.aims_event_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_4_aims_event_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.aims_event_lille;

------------------------------------------------------------------------------------------------------------------

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.aims_event_lille s1;

-- select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.5) aims_episode_lille.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.aims_episode_lille;

COPY etl.aims_episode_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_5_aims_episode_lille.csv' 
DELIMITER ';' CSV HEADER;

select * from etl.aims_episode_lille;
--

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.aims_episode_lille s1;

-- select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.6) indicator_lille.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.indicator_lille;

COPY etl.indicator_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_6_indicator_lille.csv' 
DELIMITER ';' CSV HEADER;

-- select * from etl.indicator_lille;

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.indicator_lille s1;

-- select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.7) lille_threshold.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.threshold_lille;

COPY etl.threshold_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_7_threshold_lille.csv' 
DELIMITER ';' CSV HEADER;

--

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.threshold_lille s1;

-- select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.8) lille_ccam.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.pmsi_ccam_lille;

COPY etl.pmsi_ccam_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_8_pmsi_ccam_lille.csv' 
DELIMITER ';' CSV HEADER;

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.pmsi_ccam_lille s1;

-- select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.9) lille_cim10.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.pmsi_cim10_lille;

COPY etl.pmsi_cim10_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_9_pmsi_cim10_lille.csv' 
DELIMITER ';' CSV HEADER;

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.pmsi_cim10_lille s1;

select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- 5.9 new_concept_lille.csv
------------------------------------------------------------------------------------------------------------------

delete from etl.new_concept_lille;

COPY etl.new_concept_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/5_99_concept_lille.csv' 
DELIMITER ';' CSV HEADER;

insert into etl.concept_lille
select nextval('etl.concept_lille_id_seq'), s1.concept_name, s1.domain_id, 
s1.vocabulary_id, s1.concept_class_id, s1.standard_concept, 
s1.concept_code, s1.valid_start_date, s1.valid_end_date, s1.invalid_reason
from etl.new_concept_lille s1;

select * from etl.concept_lille order by concept_id desc;

------------------------------------------------------------------------------------------------------------------
-- Insertion of local concepts into concept
------------------------------------------------------------------------------------------------------------------

delete from omop.concept where concept_id >= 2000002000;

delete from omop.concept where concept_id in (select concept_id from etl.concept_lille);

insert into omop.concept
select * from etl.concept_lille;

-- select vocabulary_id, count(*) from etl.concept_lille group by vocabulary_id;

------------------------------------------------------------------------------------------------------------------
-- 6) 6_aims_concept_relationship.csv
------------------------------------------------------------------------------------------------------------------

-- 6.1) Manual mapping

delete from etl.prop_concept_relationship_lille;

copy etl.prop_concept_relationship_lille
from '/home/ant/Documents/01_projets/omop_anesthesia/etl/02_etl_scripts/standardized_vocabularies/6_mapping_concept_relationship.csv' 
delimiter ';' csv header;

select * from etl.prop_concept_relationship_lille;

--

delete from etl.temp_concept_relationship;

insert into etl.temp_concept_relationship
select c1.concept_id as concept_id_1
	, c2.concept_id as concept_id_2
	, prop.relationship_id
	, prop.valid_start_date
	, prop.valid_end_date
	, prop.invalid_reason
from etl.prop_concept_relationship_lille prop
left outer join omop.concept c1
on prop.local_vocabulary_id = c1.vocabulary_id
and prop.local_concept_code = c1.concept_code
left outer join omop.concept c2
on prop.standard_concept_id = c2.concept_id
where prop.standard_concept_id is not null;

select * from etl.temp_concept_relationship;
--

-- Automatic mapping between CIM10 and ICD10

drop table if exists etl.concept_cim10;

create table if not exists etl.concept_cim10 as
select *
, length(replace(concept_code, '+', '')) as concept_code_len
, left(replace(concept_code, '+', ''), length(replace(concept_code, '+', '')) - 1) as concept_code_1
, left(replace(concept_code, '+', ''), length(replace(concept_code, '+', '')) - 2) as concept_code_2
, left(replace(concept_code, '+', ''), length(replace(concept_code, '+', '')) - 3) as concept_code_3
from omop.concept
where vocabulary_id = 'Lille CIM10';

drop table if exists omop_etl.concept_cim10_icd10;

CREATE TABLE if not exists etl.concept_cim10_icd10
(
  cim10_concept_id integer,
  cim10_concept_name character varying,
  cim10_concept_code character varying(50),
  cim10_concept_code_1 character varying(50),
  cim10_concept_code_2 character varying(50),
  lien character varying(50),
  icd10_concept_id integer,
  icd10_concept_name character varying,
  icd10_concept_code character varying(50),
  priorite integer
);

select * from omop_etl.concept_cim10_icd10 limit 100;

  -- Insertion lien exact

 insert into etl.concept_cim10_icd10
 select cim10.concept_id 
 , cim10.concept_name 
 , cim10.concept_code 
 , cim10.concept_code_1 
 , cim10.concept_code_2
 , 'code exact'
 , icd10.concept_id
 , icd10.concept_name
 , icd10.concept_code
 , 1
 from etl.concept_cim10 cim10
 inner join etl.concept_icd10 icd10
 on cim10.concept_code = icd10.concept_code
;

-- Lien - 1

 insert into etl.concept_cim10_icd10
 select cim10.concept_id 
 , cim10.concept_name 
 , cim10.concept_code 
 , cim10.concept_code_1 
 , cim10.concept_code_2
 , 'code -1'
 , icd10.concept_id
 , icd10.concept_name
 , icd10.concept_code
 , 2
 from etl.concept_cim10 cim10
 inner join etl.concept_icd10 icd10
 on cim10.concept_code_1 = icd10.concept_code
 where cim10.concept_id not in (select cim10_concept_id from etl.concept_cim10_icd10);

 -- Lien - 2

 insert into etl.concept_cim10_icd10
 select cim10.concept_id 
 , cim10.concept_name 
 , cim10.concept_code 
 , cim10.concept_code_1 
 , cim10.concept_code_2
 , 'code -2'
 , icd10.concept_id
 , icd10.concept_name
 , icd10.concept_code
 , 3
 from etl.concept_cim10 cim10
 inner join etl.concept_icd10 icd10
 on cim10.concept_code_2 = icd10.concept_code
 where cim10.concept_id not in (select cim10_concept_id from etl.concept_cim10_icd10)
 ;

 -- Lien - 3

insert into etl.concept_cim10_icd10
select cim10.concept_id 
, cim10.concept_name 
, cim10.concept_code 
, cim10.concept_code_1 
, cim10.concept_code_2
, 'code -3'
, icd10.concept_id
, icd10.concept_name
, icd10.concept_code
, 3
from etl.concept_cim10 cim10
inner join etl.concept_icd10 icd10
on cim10.concept_code_3 = icd10.concept_code
where cim10.concept_id not in (select cim10_concept_id from etl.concept_cim10_icd10)
;

-- CIM10 -> ICD10

insert into etl.temp_concept_relationship
select
	cim10_concept_id as concept_id_1
	, icd10_concept_id as concept_id_2
	, 'Lille CIM10 to ICD10'
	, '2020-01-01'
	, '2099-12-31'
	, null::text
from etl.concept_cim10_icd10;

-- ICD10 -> CIM10
insert into etl.temp_concept_relationship
select
	icd10_concept_id as concept_id_1
	, cim10_concept_id as concept_id_2
	, 'ICD10 to Lille CIM10'
	, '2020-01-01'
	, '2099-12-31'
	, null::text
from etl.concept_cim10_icd10;

--

select * from omop.concept_relationship t1 
where exists (select * from etl.temp_concept_relationship t2 
			  where t1.concept_id_1 = t2.concept_id_1
			  and t1.relationship_id = t2.relationship_id
			 );
			 
-- ajouter suppression quand le type de relation a été créé ici			 
			 
delete from omop.concept_relationship t1 
where exists (select * from etl.temp_concept_relationship t2
			  where t1.concept_id_1 = t2.concept_id_1
			  and t1.relationship_id = t2.relationship_id
			 );
			 
insert into omop.concept_relationship
select * from etl.temp_concept_relationship;
--

select relationship_id, count(*)
from etl.temp_concept_relationship
group by relationship_id;