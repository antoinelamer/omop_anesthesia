--

-- Queries : activity
--

-- Number of operations

select count(*)
from omop.visit_detail vd
where vd.visit_detail_concept_id = 2100000000;

-- Number of passages in Post-Anesthesia Care Unit
select count(*)
from omop.visit_detail vd
where vd.visit_detail_concept_id = 2100000001;

-- Pivot operations



delete from reporting.operation;

with operation as(
	select person_id
	, visit_occurrence_id
	, visit_detail_id
	, visit_detail_start_date
	, extract(year from visit_detail_start_date)::integer as year
	, care_site_id
	from omop.visit_detail vd
	where vd.visit_detail_concept_id = 2100000000
), age as (
	select o.visit_detail_id, oage.value_as_number as age
	from operation o 
	left outer join omop.observation oage
	on o.visit_detail_id = oage.visit_detail_id
	where oage.observation_concept_id = 4265453
), sex as (
	select o.visit_detail_id, c.concept_name as sex
	from operation o 
	inner join omop.person psex
	on o.person_id = psex.person_id
	left outer join omop.concept c
	on psex.gender_concept_id = c.concept_id	
), asa as (
	select o.visit_detail_id, oasa.value_as_number as asa
	from operation o 
	left outer join omop.observation oasa
	on o.visit_detail_id = oasa.visit_detail_id
	where oasa.observation_concept_id = 4185946
), operation_priority as (
	select o.visit_detail_id, 
	case 
		when value_as_concept_id = 4014167
		then 'Urgent'
		else 'Normal priority'
	end as operation_priority
	from operation o
	left outer join omop.observation oop
	on o.visit_detail_id = oop.visit_detail_id
	where oop.observation_concept_id = 4156188
), weight as (
	select o.visit_detail_id, oweight.value_as_number as weight
	from operation o 
	left outer join omop.observation oweight
	on o.visit_detail_id = oweight.visit_detail_id
	where oweight.observation_concept_id = 37111521
), bmi as (
	select o.visit_detail_id, obmi.value_as_number as bmi
	from operation o 
	left outer join omop.observation obmi
	on o.visit_detail_id = obmi.visit_detail_id
	where obmi.observation_concept_id = 4245997
), operating_room_name as (
	select o.visit_detail_id, room.care_site_name operating_room_name
	from operation o
	inner join omop.care_site room
	on o.care_site_id = room.care_site_id
), anesthesia_unit_name as (
	select o.visit_detail_id, cs.care_site_name as anesthesia_unit_name
	from operation o
	inner join omop.fact_relationship fr
	on o.care_site_id = fr.fact_id_1
	and fr.relationship_concept_id = 46233688
	inner join omop.care_site cs
	on fr.fact_id_2 = cs.care_site_id
), operating_room_passage as (
	select o.visit_detail_id
	, coalesce(1::integer, 0::integer) as operating_room_passage
	, round(EXTRACT(epoch FROM (episode_end_datetime - episode_start_datetime)) / 60) as operating_room_duration 
	from operation o
	left outer join omop.episode e
	on o.visit_detail_id = e.visit_detail_id
	where e.episode_concept_id = 4021813
), pacu_passage as (
	select o.visit_detail_id
	, coalesce(1::integer, 0::integer) as pacu_passage
	, round(EXTRACT(epoch FROM (episode_end_datetime - episode_start_datetime)) / 60) as pacu_duration 
	from operation o
	left outer join omop.episode e
	on o.visit_detail_id = e.visit_detail_id
	where e.episode_concept_id = 4134563
), death as (
	select o.visit_detail_id, 1::integer as death
	from operation o
	inner join omop.observation obs
	on o.visit_occurrence_id = obs.visit_occurrence_id
	where obs.observation_concept_id = 4306655
), postop as (
	select o.visit_detail_id
	, vo.visit_end_date - vo.visit_start_date as visit_duration
	, case 
	when o.visit_occurrence_id is null then null::integer
	when death.death is not null then 1::integer
	else 0::integer
	end as death
	from operation o
	inner join omop.visit_occurrence vo
	on o.visit_occurrence_id = vo.visit_occurrence_id
	left outer join death
	on o.visit_detail_id = death.visit_detail_id
)
insert into reporting.operation
select o.person_id::integer
		, o.visit_occurrence_id::integer
		, o.visit_detail_id::integer
		, o.visit_detail_start_date::date
		, o.year::integer
		, operating_room_name.operating_room_name
		, anesthesia_unit_name.anesthesia_unit_name
		, age.age::integer, sex.sex, asa.asa, operation_priority.operation_priority
		, weight.weight, bmi.bmi
		, coalesce(operating_room_passage.operating_room_passage, 0::integer) as operating_room_passage
		, operating_room_passage.operating_room_duration::integer
		, coalesce(pacu_passage.pacu_passage, 0::integer) as pacu_passage
		, pacu_passage.pacu_duration::integer
		, postop.visit_duration
		, postop.death
from operation o
left outer join age
on o.visit_detail_id = age.visit_detail_id
left outer join sex
on o.visit_detail_id = sex.visit_detail_id
left outer join asa
on o.visit_detail_id = asa.visit_detail_id
left outer join operation_priority
on o.visit_detail_id = operation_priority.visit_detail_id
left outer join weight
on o.visit_detail_id = weight.visit_detail_id
left outer join bmi
on o.visit_detail_id = bmi.visit_detail_id
left outer join operating_room_name
on o.visit_detail_id = operating_room_name.visit_detail_id
left outer join anesthesia_unit_name
on o.visit_detail_id = anesthesia_unit_name.visit_detail_id
left outer join operating_room_passage
on o.visit_detail_id = operating_room_passage.visit_detail_id
left outer join pacu_passage
on o.visit_detail_id = pacu_passage.visit_detail_id
left outer join postop
on o.visit_detail_id = postop.visit_detail_id
;

select * from reporting.operation 
--where visit_detail_id = 474298
--where visit_duration > 0
limit 100;

select operation_priority, count(*)
from reporting.operation
group by operation_priority;

