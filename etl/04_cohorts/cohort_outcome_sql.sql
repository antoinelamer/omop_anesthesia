----------------------------------------------
--
-- Outcome
--
--
--
--
--
----------------------------------------------

--
CREATE TABLE if not exists cohort.cohort
(
    cohort_definition_id integer,
    subject_id bigint,
    cohort_start_date date,
    cohort_end_date date
)
WITH (
    OIDS = FALSE
)
TABLESPACE tbs_crypt;

ALTER TABLE cohort.cohort
    OWNER to postgres;



-- Décès
----------------------------------------

insert into cohort.cohort
select 
3 as cohort_definition_id
, o.person_id as subject_id
, observation_end_date as cohort_start_date
, observation_end_date as cohort_end_date
from omop.observation o
where o.observation_concept_id = 4306655
;