
-- Load outcome

delete from @resultsDatabaseSchema.cohort where cohort_definit_id = 3;

insert into cohort.cohort
select 
3 as cohort_definition_id
, o.person_id as subject_id
, observation_date as cohort_start_date
, observation_date as cohort_end_date
from omop.observation o
where o.observation_concept_id = 4306655
;