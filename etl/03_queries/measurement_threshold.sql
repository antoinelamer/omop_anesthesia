---


--
-- Procedure occurrence
--

select c.concept_name, b_temps_hors_seuil, count(*)
from raw_data.interv_seuil ie
inner join omop.concept c
on ie.id_seuil::text = c.concept_code
and c.vocabulary_id = 'Lille Threshold'
group by c.concept_name, b_temps_hors_seuil
order by count(*);

limit 100;

-- Extract

select count(*) from etl.procedure_occurrence_extract;
select * from etl.procedure_occurrence_extract limit 100;

-- TR1
-----------------------------------------------------------------------
select count(*) from etl.procedure_occurrence_tr1;

-- Join with visit_detail

select poe.visit_source_value
from etl.procedure_occurrence_extract poe
left outer join omop.visit_detail vd
on vd.visit_detail_source_value = poe.visit_detail_source_value
and vd.visit_detail_type_concept_id = 32034
where vd.person_id is null
;

select * from raw_data.sejour where iep = '10308833' limit 100;
select * from raw_data.intervention_patient where id_patient = 190584

select * from raw_data.intervention_patient where iep like '%10308833%'

select * from omop.person where person_source_value = '190584'

select count(*) from etl.procedure_occurrence_tr1 where person_id = 0;
select count(*) from etl.procedure_occurrence_tr1 where visit_occurrence_id = 0;

-- TR2
-----------------------------------------------------------------------
select count(*) from etl.procedure_occurrence_tr2;

-- 
select count(*) from etl.condition_occurrence_tr2 where condition_source_concept_id = 0;

select measurement_source_value, count(*) 
from etl.measurement_threshold_tr
where measurement_source_concept_id = 0
group by measurement_source_value
order by count(*) desc;

select concept_name, count(*) 
from etl.measurement_threshold_tr tr, omop.concept c
where tr.measurement_source_concept_id = c.concept_id
group by concept_name
order by count(*) desc;

-- TR3

select distinct condition_source_value
from etl.condition_occurrence_tr3
where condition_concept_id = 0
limit 100;


select condition_source_value, SPLIT_PART(condition_source_value, ' ', 1)
from etl.condition_occurrence_tr2 where condition_source_concept_id = 0
