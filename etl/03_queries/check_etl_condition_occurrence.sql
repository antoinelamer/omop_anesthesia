---


--
-- condition_occurrence
--

select * from raw_data.diagnostic limit 100;

-- Extraxct

select count(*) from etl.condition_occurrence_extract;

select condition_type_source_value, count(*) 
from etl.condition_occurrence_extract
group by condition_type_source_value;

-- TR1
-----------------------------------------------------------------------
select count(*) from etl.condition_occurrence_tr1;

-- Join with visit_detail

select count(*) from etl.condition_occurrence_tr1 where person_id = 0;
select count(*) from etl.condition_occurrence_tr1 where visit_occurrence_id = 0;

select count(*) from etl.condition_occurrence_tr1 where condition_start_date is null;
select count(*) from etl.condition_occurrence_tr1 where condition_end_date is null;

-- condition_type_concept_id

select c.concept_name, tr1.condition_type_concept_id, count(*)
from etl.condition_occurrence_tr1 tr1, omop.concept c
where tr1.condition_type_concept_id = c.concept_id
group by c.concept_name, tr1.condition_type_concept_id;

-- TR2
-----------------------------------------------------------------------
select count(*) from etl.condition_occurrence_tr2;

-- 
select count(*) from etl.condition_occurrence_tr2 where condition_source_concept_id = 0;

select condition_source_value, count(*) 
from etl.condition_occurrence_tr2 where condition_source_concept_id = 0
group by condition_source_value
order by count(*) desc;

-- TR3

select distinct condition_source_value
from etl.condition_occurrence_tr3
where condition_concept_id = 0
limit 100;


select condition_source_value, SPLIT_PART(condition_source_value, ' ', 1)
from etl.condition_occurrence_tr2 where condition_source_concept_id = 0
