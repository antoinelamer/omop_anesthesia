----------------------------------------
--
-- OMOP ANESTHESIA
--
-- Antoine Lamer
--
-- 08/12/19
--
-- Creation of schemas
--
-- raw_data = schema with raw data
-- etl = schema for etl transformations
-- omop = final OMOP schema
--
----------------------------------------

-- SCHEMA: raw_data
----------------------------------------

-- DROP SCHEMA raw_data ;

CREATE SCHEMA raw_data AUTHORIZATION postgres;
	
	
-- SCHEMA: etl
----------------------------------------

-- DROP SCHEMA etl ;

CREATE SCHEMA etl AUTHORIZATION postgres;	
	
	
-- SCHEMA: omop
----------------------------------------

-- DROP SCHEMA omop ;

CREATE SCHEMA omop AUTHORIZATION postgres;
	
-- SCHEMA: achilles
----------------------------------------

-- DROP SCHEMA achilles ;

CREATE SCHEMA achilles AUTHORIZATION postgres;



-- Create as many cohort shema as you need
-- SCHEMA: cohort_
----------------------------------------

-- DROP SCHEMA cohort ;

CREATE SCHEMA cohort AUTHORIZATION postgres;

-- DROP SCHEMA cohort_ ;

CREATE SCHEMA cohort_suf AUTHORIZATION postgres;

-- 

CREATE SCHEMA result AUTHORIZATION postgres;