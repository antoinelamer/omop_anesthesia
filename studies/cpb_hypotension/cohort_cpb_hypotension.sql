

IF OBJECT_ID('@cohortDbSchema.cohort_cpb_hypotension', 'U') IS NOT NULL
DROP TABLE @cohortDbSchema.cohort_cpb_hypotension;

SELECT ccv.*
INTO @cohortDbSchema.cohort_cpb_hypotension
FROM (
select
1 as cohort_definition_id,
vd.visit_occurrence_id as subject_id, 
visit_detail_start_date as cohort_start_date,
visit_detail_end_date as cohort_end_date
from @cdmDatabaseSchema.visit_detail vd
-- join care site for selecting surgical unit
inner join @cdmDatabaseSchema.fact_relationship fr
on vd.care_site_id = fr.fact_id_1
and fr.relationship_concept_id = 46233688
inner join @cdmDatabaseSchema.care_site cs
on fr.fact_id_2 = cs.care_site_id
-- join episode for detecting cardiopulomnary bypass period
inner join omop.episode e
on vd.visit_detail_id = e.visit_detail_id
and e.episode_source_concept_id = (select min(concept_id) from omop.concept 
  where concept_name = 'Episode de CEC (delta PAD-PAS < 15mmHg - PAD < 150 mmHg)')
where vd.visit_detail_concept_id = (select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit')
and cs.care_site_name = 'CCV'
and year(vd.visit_detail_start_date) between 2010 and 2018
) ccv