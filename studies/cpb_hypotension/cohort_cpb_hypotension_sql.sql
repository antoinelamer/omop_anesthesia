----------------------------------------
--
-- Extracoporeal circulation
--
-- Antoine Lamer
--
-- 2020-03-29
--
-- Dev
--
----------------------------------------

-- Monitorage pression invasive 
-- Period
-- period_concept_id = 2000003062	

-- Episode de CEC
-- Episode
-- episode_concept_id = 2000003048	

-- Hypotension < 65 mmHg
-- Episode
-- Episode_source_concept_id = 2000004822

-- Mortalité
-- Observation
-- observation_concept_id = 4306655

/*
CCV : 27473
CCV + CEC 5859
*/

drop table if exists cohort.cpb_procedure;

create table if not exists cohort.cpb_procedure as
select vd.person_id
, vd.visit_detail_id as visit_detail_id
, visit_detail_start_date as cohort_start_date
, visit_detail_end_date as cohort_end_date
from omop.visit_detail vd 
-- join fact_relationship
inner join omop.fact_relationship fr
on vd.care_site_id = fr.fact_id_1
and fr.relationship_concept_id = 46233688
-- join CCV
inner join omop.care_site cs
on fr.fact_id_2 = cs.care_site_id
and cs.care_site_name = 'CCV'
-- join CEC
inner join omop.episode e
on vd.visit_detail_id = e.visit_detail_id
and e.episode_source_concept_id = (select min(concept_id) from omop.concept where concept_name = 'Episode de CEC (delta PAD-PAS < 15mmHg - PAD < 150 mmHg)')
-- filter operating room and date
where vd.visit_detail_concept_id = (select min(concept_id) from omop.concept where concept_name = 'Operating Room Visit')
and extract(year from visit_detail_start_date) between 2010 and 2018
;

select cpb.*
from cohort.cpb_procedure cpb
inner join omop.measurement m
on cpb.visit_detail_id = m.visit_detail_id
and m.measurement_source_concept_id =
(select concept_id from omop.concept where concept_name = 'PASm < 65 mmHg - Monitorage Invasif - 60 - Ech 30 - Répétition - Pmax 25')
and value_as_concept_id = 4188539
;


select distinct value_as_concept_id
from omop.measurement
where measurement_source_concept_id =
(select concept_id from omop.concept where concept_name = 'PARTm < 65 mmHg - Monitorage Invasif - 60 - Ech 30 - Répétition - Pmax 25')
and value_as_concept_id = 4188539



drop table if exists cohort.cpb_select;
create table if not exists cohort.cpb_select as
select *
from cohort.cpb_procedure 
where person_id not in (
select person_id
from cohort.cpb_procedure
group by person_id having count(*) > 1);

-- Exposition
----------------------------------------

select value_as_concept_id, count(*)
from cohort.cpb_select cpb
inner join omop.measurement m
on cpb.visit_detail_id = m.visit_detail_id
and m.measurement_source_concept_id = 2000004460
group by value_as_concept_id;

-- select * from 

select *
from omop.concept where vocabulary_id = 'Lille Threshold'
and concept_name = 'PARTm < 65 mmHg - Monitorage Invasif - 60 - Ech 30 - Répétition - Pmax 25'
order by concept_name;


-- Death
-- 4306655

select *
from omop.observation
where observation_concept_id =4306655;

-- Hypotension
--

select * from omop.concept where concept_name = 'PNIm < 65 mmHg - Anesthésie - 360 - Ech 300 - Répétition - Pmax 25'

select c.concept_name, count(distinct m.visit_detail_id)
from omop.measurement m, omop.concept c
where m.measurement_source_concept_id = c.concept_id
group by c.concept_name
order by c.concept_name











IF OBJECT_ID('@cohortDbSchema.cohort', 'U') IS NOT NULL
DROP TABLE @cohortDbSchema.cohort_ccv;

SELECT first_use.*
INTO @cohortDbSchema.cohort_ccv
FROM (
select
1 as cohort_definition_id,
vd.visit_occurrence_id as subject_id, 
visit_detail_start_date as cohort_start_date,
visit_detail_end_date as cohort_end_date
from @cdmDatabaseSchema.visit_detail vd,  @cdmDatabaseSchema.fact_relationship fr, @cdmDatabaseSchema.care_site cs
where vd.care_site_id = fr.fact_id_1
and fr.fact_id_2 = cs.care_site_id
and vd.visit_detail_concept_id = 2100000000
and relationship_concept_id = 46233688
and cs.care_site_name = 'CCV'
) first_use