----------------------------------------
--
-- Extracoporeal circulation
--
-- Antoine Lamer
--
-- 2020-03-29
--
-- Dev
--
----------------------------------------

-- Monitorage Pression invasive

2000003062	"Monitorage Pression Invasive"
2000003048	"Episode de CEC (delta PAD-PAS < 15mmHg - PAD < 150 mmHg)"
select * from 

-- CEC
----------------------------------------

select *
from omop.concept where vocabulary_id = 'Lille Threshold';

select visit_detail_id, count(*) 
from omop.episode where episode_source_concept_id = 2000003048
group by visit_detail_id
having count(*) > 1;

-- Death
-- 4306655

select *
from omop.observation
where observation_concept_id =4306655;











IF OBJECT_ID('@cohortDbSchema.cohort', 'U') IS NOT NULL
DROP TABLE @cohortDbSchema.cohort_ccv;

SELECT first_use.*
INTO @cohortDbSchema.cohort_ccv
FROM (
select
1 as cohort_definition_id,
vd.visit_occurrence_id as subject_id, 
visit_detail_start_date as cohort_start_date,
visit_detail_end_date as cohort_end_date
from @cdmDatabaseSchema.visit_detail vd,  @cdmDatabaseSchema.fact_relationship fr, @cdmDatabaseSchema.care_site cs
where vd.care_site_id = fr.fact_id_1
and fr.fact_id_2 = cs.care_site_id
and vd.visit_detail_concept_id = 2100000000
and relationship_concept_id = 46233688
and cs.care_site_name = 'CCV'
) first_use