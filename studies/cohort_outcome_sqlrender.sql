
-- Load outcome

-- Death

delete from @cohortDbSchema.cohort where cohort_definition_id = 3;

insert into @cohortDbSchema.cohort
select 
3 as cohort_definition_id
, o.person_id as subject_id
, observation_date as cohort_start_date
, observation_date as cohort_end_date
from @cdmDatabaseSchema.observation o
where o.observation_concept_id = 4306655
;

-- Other outcomes