# Anesthesia data and OMOP CDM

This repository contains two developments about anesthesia data with OMOP CDM.


| Project | Description | Progress |
| -------- | -------- | -------- |
| **Dashboards**    | Anesthesia dashboards      |  In development  |
| **ETL**			| Conceptual and structural mappings of anesthesia data into OMOP CDM | In development |

The [wiki](https://framagit.org/antoine.lamer/omop_anesthesia/-/wikis/home) provides documentation for these two projects.

## ETL

![Structural mapping](https://gitlab.com/antoinelamer/omop_anesthesia/-/raw/master/images/anesthesia_mapping.png)

## Dashboard

![First dashboard about description of the population](https://framagit.org/antoine.lamer/omop_anesthesia/raw/master/images/dashboard_population.png)

![](https://framagit.org/antoine.lamer/omop_anesthesia/raw/master/images/dashboard_ventilation.png)

## Vocabularies

Selected concepts related to operating room, haemodynamics, and ventilatory management.